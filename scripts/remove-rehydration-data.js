const fs = require("fs");
const cheerio = require("cheerio");


if (process.argv.length !== 3)
{
  console.error("Filename required.");
  process.exit(1);
}

let filename = process.argv[2];
console.log(`Processing ${filename}`);

let file = fs.readFileSync(filename, "utf8");

let html = cheerio.load(file);

// Remove __NEXT_DATA__
html("#__NEXT_DATA__").remove();


html("script").each((index, element) =>
{
  let src = element.attribs["src"];
  if (src && src.startsWith("/_next/"))
  {
    html(element).remove();
  }
});

fs.writeFileSync(filename, html.html(), "utf8");

