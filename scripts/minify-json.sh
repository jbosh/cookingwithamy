#!/usr/bin/env bash

filename="$1"
if [[ ! -f "$filename" ]]; then
  echo "Could not find $filename."
  exit 1
fi

content="$(jq -c . < "$filename")"
printf "%s" "$content" > "$filename"