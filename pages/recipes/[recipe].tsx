import { ItemList, prepareRecipe, RecipeMatter } from "lib/types";
import { getFilenameWithoutExtension, getMatterPath, listMatterDirectory, readMatter } from "lib/offlineUtilities";
import { classNames } from "lib/utilities";
import { GetStaticPropsContext, GetStaticPropsResult } from "next";
import Head from "next/head";
import ReactMarkdown from "react-markdown";
import Footer from "lib/components/footer";
import Header from "lib/components/header";

import "../../css/bass.css";
import "../../css/recipe.scss";

interface RecipeProps extends RecipeMatter
{
  components: RecipeMatter[] | null;
}

function renderIngredients(ingredients: ItemList[]): JSX.Element[]
{
  return ingredients.map(g => <ul className="ingredients" key={g.heading}>
    {g.heading && <h5>{g.heading}</h5>}
    {g.items.map(i => <li key={i}>
      <label>
        <input type="checkbox" />
        <ReactMarkdown>{i}</ReactMarkdown>
      </label>
    </li>
    )}
  </ul>);
}

function renderDirections(directions: ItemList[]): JSX.Element[]
{
  return directions.map(g => <ol key={g.heading}>
    {g.heading && <h5>{g.heading}</h5>}
    {g.items.map(i => <li key={i}>
      <label>
        <ReactMarkdown>{i}</ReactMarkdown>
      </label>
    </li>
    )}
  </ol>);
}

function renderComponents(components: RecipeMatter[]): JSX.Element
{
  let componentSize: string;
  switch (components.length)
  {
    case 1:
      componentSize = "sm-col-12";
      break;
    case 2:
      componentSize = "sm-col-6";
      break;
    default:
      componentSize = "sm-col-4";
      break;
  }

  return <>
    <div className="components bg-darken-2 p2 mt3 mb3 center">
      &darr; Components &darr;
    </div>
    <div className="clearfix mxn2">
      {components.map(component =>
        <div className={classNames("sm-col px2", componentSize)} key={component.filename}>
          <h4 className="blue center" id={component.filename}>{component.title}</h4>
          <div className="image ratio bg-cover" style={{ backgroundImage: `url(/images/${component.image})` }}>
            <img className="hide" src={`/images/${component.image}`} alt="Photo" />
          </div>

          {component.imageCredit && <a href={component.imageCredit} className="right">
            <svg className="js-geomicon geomicon" width="14" height="14" data-icon="camera" viewBox="0 0 32 32" style={{ fill: "currentcolor" }}>
              <title>camera icon</title>
              <path d="M0 6 L8 6 L10 2 L22 2 L24 6 L32 6 L32 28 L0 28 z M9 17 A7 7 0 0 0 23 17 A7 7 0 0 0 9 17"></path>
            </svg>
          </a>}


          <h4 className="blue mt1 mb2 xs-center">Ingredients</h4>
          {component.ingredients && renderIngredients(component.ingredients)}

          <h4 className="blue mt1 mb2 xs-center">Directions</h4>
          {component.directions && renderDirections(component.directions)}
        </div>
      )}
    </div>
  </>
}

export default function Recipe(props: RecipeProps)
{
  return <div className="container">
    <Head>
      <title>{`${props.title}: Cooking with Amy`}</title>
    </Head>
    <Header />

    <div className="xs-p2">
      <img className="main-image" src={`/images/${props.image}`} alt="Picture of food." />
      {props.imageCredit &&
        <a href={props.imageCredit} className="right">
          <svg className="js-geomicon geomicon" width="14" height="14" data-icon="camera" viewBox="0 0 32 32" style={{ fill: "currentcolor" }}>
            <title>camera icon</title>
            <path d="M0 6 L8 6 L10 2 L22 2 L24 6 L32 6 L32 28 L0 28 z M9 17 A7 7 0 0 0 23 17 A7 7 0 0 0 9 17"></path>
          </svg>
        </a>
      }
    </div>

    <article className="post-content px2">
      <header className="post-header">
        <h1 className="post-title center m1 sm-mt3">{props.title}</h1>
      </header>

      {props.experimental &&
        <div className="px2 mt3 clearfix">
          <div className="sm-col-8 mx-auto warning">
            <img src="/images/warning.svg" alt="Warning" />
            This recipe has not been tried and experimental.
          </div>
        </div>
      }

      <div className="px2 mt3 clearfix">
        <div className="sm-col-8 mx-auto"><ReactMarkdown>{props.content}</ReactMarkdown></div>
      </div>

      {props.tips &&
        <div className="clearfix">
          <h4 className="blue">Tips</h4>
          <div className="sm-col sm-col-12 lg-col-12">
            <ul>
              {props.tips.map((t, i) => <li key={i}><ReactMarkdown>{t}</ReactMarkdown></li>)}
            </ul>
          </div>
        </div>
      }

      <div className="mt3 clearfix">
        <h4 className="blue mt0 mb2 xs-center">Information</h4>
        <div className="sm-col sm-col-12 lg-col-12 fields">
          <ul>
            {props.prepTime && <li><b>Prep Time: </b>{props.prepTime}</li>}
            {props.cookTime && <li><b>Cook Time: </b>{props.cookTime}</li>}
            {props.totalTime && <li><b>Total Time: </b>{props.totalTime}</li>}
            {props.size && <li><b>Makes: </b>{props.size}</li>}
          </ul>
        </div>
      </div>

      {props.storage &&
        <div className="mt3 clearfix storage">
          <h4 className="blue mt0 mb2 xs-center">Storage Information</h4>
          <div className="sm-col sm-col-12 lg-col-12">
            {props.storage.store &&
              <div>
                <h4>To Store</h4>
                <ReactMarkdown>{props.storage.store}</ReactMarkdown>
              </div>
            }

            {props.storage.reheat &&
              <div>
                <h4>To Reheat</h4>
                <ReactMarkdown>{props.storage.reheat}</ReactMarkdown>
              </div>
            }

            {props.storage.freeze &&
              <div>
                <h4>To Freeze</h4>
                <ReactMarkdown>{props.storage.freeze}</ReactMarkdown>
              </div>
            }
          </div>
        </div>
      }

      <div className="clearfix mt3">
        <div className="sm-col sm-col-6 lg-col-6">
          {props.components &&
            <>
              <h4 className="blue mt0 mb2 xs-center">Components</h4>
              <ul>
                {props.components.map(c => <li key={c.filename}><a href={`#${c.filename}`}>{c.title}</a></li>)}
              </ul>
            </>
          }

          <h4 className="blue mt1 mb2 xs-center">Ingredients</h4>
          {props.ingredients && renderIngredients(props.ingredients)}
        </div>

        <div className="sm-col sm-col-6 lg-col-6">
          <h4 className="blue mt1 mb2 xs-center">Directions</h4>
          {props.directions && renderDirections(props.directions)}
        </div>

      </div>

      {props.components && renderComponents(props.components)}

      {props.categories?.map(c => <p className="clearfix" key={c}>Category: <span>{c}</span></p>)}
    </article>
    <Footer recipeName={props.title} />
  </div>;
}

export async function getStaticProps(context: GetStaticPropsContext): Promise<GetStaticPropsResult<RecipeProps>>
{
  let slug = context.params?.recipe as string ?? "readme.html";
  let filename = getFilenameWithoutExtension(slug) + ".md";
  let matter = prepareRecipe(readMatter(getMatterPath(`recipes/${filename}`)) as RecipeMatter);
  let components: RecipeMatter[] | null = null;
  if (matter.componentNames)
  {
    components = matter.componentNames.map(c => prepareRecipe(readMatter(getMatterPath(`recipes/components/${c}.md`)) as RecipeMatter));
  }

  return {
    props: {
      ...matter,
      components,
    },
  };
}

export async function getStaticPaths()
{
  let recipes = listMatterDirectory("recipes")
    .map(f => process.env.NODE_ENV === "production"
      ? `/recipes/${getFilenameWithoutExtension(f)}`
      : `/recipes/${getFilenameWithoutExtension(f)}.html`);
  return { paths: recipes, fallback: false };
}