import { PostMatter } from "lib/types";
import { getFilenameWithoutExtension, getMatterPath, listMatterDirectory, Matter, readMatter, readMatterDirectory } from "lib/offlineUtilities";
import { GetStaticPropsContext, GetStaticPropsResult } from "next";
import Head from "next/head";
import ReactMarkdown from "react-markdown";
import Footer from "lib/components/footer";
import Header from "lib/components/header";

import "../../css/bass.css";
import "../../css/recipe.scss";

export default function Post(props: PostMatter)
{
  return <div className="container">
    <Head>
      <title>{`${props.title}: Cooking with Amy`}</title>
    </Head>
    <Header />
    <div className="post container max-width-2 px2">
      <h1>{props.title}</h1>
      <ReactMarkdown>{props.content}</ReactMarkdown>
      <p className="meta bg-darken-1 p2 mb4">Posted by <a href={`https://gitlab.com/${props.author}`}>@{props.author}</a> on {props.date?.toLocaleString()}</p>
    </div>
    <Footer />
  </div>;
}

export async function getStaticProps(context: GetStaticPropsContext): Promise<GetStaticPropsResult<PostMatter>>
{
  let slug = context.params?.post as string ?? "readme.html";
  let filename = getFilenameWithoutExtension(slug) + ".md";
  let matter = readMatter(getMatterPath("posts/" + filename)) as PostMatter;

  return {
    props: {
      ...matter,
    },
  };
}

export async function getStaticPaths()
{
  let recipes = (readMatterDirectory("posts") as PostMatter[])
    .map(f => process.env.NODE_ENV === "production"
      ? `/posts/${f.url}`
      : `/posts/${f.url}.html`);
  return { paths: recipes, fallback: false };
}