import { prepareRecipe, RecipeMatter } from "lib/types";
import { readMatterDirectory } from "lib/offlineUtilities";
import { NextApiRequest, NextApiResponse } from "next";

function jsonifyArray(arr: string | string[] | undefined | null): string | null
{
  if (!arr)
    return null;

  if (Array.isArray(arr))
    return arr.join(", ");

  return arr;
}
export default function SearchJson(req: NextApiRequest, res: NextApiResponse)
{
  let recipes = (readMatterDirectory("recipes") as RecipeMatter[]).map(prepareRecipe);

  let result = [];
  for (let recipe of recipes)
  {
    if (recipe.draft)
      continue;

    let payload = {
      title: recipe.title,
      ingredients: jsonifyArray(recipe.ingredients?.flatMap(g => g.items)),
      tags: jsonifyArray(recipe.tags),
      categories: jsonifyArray(recipe.categories),
      image: `/images/thumbs/${recipe.image}`,
      url: `/recipes/${recipe.url}.html`,
    };

    result.push(payload);
  }

  res.setHeader('Content-Type', 'application/json; charset=utf-8');
  res.write(JSON.stringify(result));
  res.end();
}