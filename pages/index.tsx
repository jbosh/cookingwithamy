import { readMatterDirectory } from "lib/offlineUtilities";
import { prepareRecipe, RecipeMatter } from "lib/types";
import IndexContainer from "./_indexContainer";

interface Props
{
  recipes: RecipeMatter[];
}

export default function Recipes(props: Props)
{
  return <IndexContainer>
    <div className="container recipes mb4 col col-4">
      <div className="clearfix p1">
        {props.recipes
          .map(r =>
            <div className="recipe" key={r.title}>
              <a className="block relative mx1 text-decoration-none black" href={`/recipes/${r.url}.html`}>
                <div className="image ratio bg-primary bg-cover" style={{ backgroundImage: `url(${`/images/thumbs/${r.image}`})` }}></div>
                <h3 className="title pt1 m0 bold">{r.title}</h3>
              </a>
            </div>
          )}
      </div>
    </div>
  </IndexContainer>;
}

export function getStaticProps(): { props: Props }
{
  let recipes = (readMatterDirectory("recipes") as RecipeMatter[])
    .map(prepareRecipe)
    .filter(r => !r.draft);

  return {
    props: {
      recipes,
    }
  };
}