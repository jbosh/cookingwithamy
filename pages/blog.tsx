import { readMatterDirectory } from "lib/offlineUtilities";
import { PostMatter, prepareRecipe, RecipeMatter } from "lib/types";

import IndexContainer from "./_indexContainer";

interface Props
{
  posts: PostMatter[];
}

export default function Blog(props: Props)
{
  return <IndexContainer>
    <div className="container blog col col-4">
      <div className="posts max-width-2 mx-auto p2">
        {props.posts.map(p =>
          <a className="post card mb2 block" href={`/posts/${p.url}.html`} key={p.title}>
            <h3 className="my1">{p.title}</h3>
            <p className="mt1">{p.content}</p>
            <p className="action">View Post</p>
          </a>)}
      </div>
    </div>
  </IndexContainer>;
}

export function getStaticProps(): { props: Props }
{
  let posts = (readMatterDirectory("posts") as PostMatter[])
    .filter(p => !p.draft)

  return {
    props: {
      posts,
    }
  };
}