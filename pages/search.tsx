import Script from "next/script";
import IndexContainer from "./_indexContainer";

export default function Search()
{
  return <IndexContainer>
    <div className="container search col col-4">
      <div className="search container p2">
        <div id="search-container">
          <input type="text" id="search-input" placeholder="Search titles, ingredients, or tags..." />
        </div>
      </div>

      <div className="clearfix">
        <div className="recipes p1" id="results-container">
        </div>
      </div>
    </div>

    {/* eslint-disable-next-line @next/next/no-sync-scripts */}
    <script src="/plugins/simple-jekyll-search.min.js" />

    <script id="search-script" dangerouslySetInnerHTML={{
      __html: `SimpleJekyllSearch({
        searchInput: document.getElementById("search-input"),
        resultsContainer: document.getElementById("results-container"),
        json: "/api/search.json",
        searchResultTemplate: '<div class="recipe sm-col sm-col-6 lg-col-4 py1 pb2"><a class="block black relative mx1 text-decoration-none" href="{url}"><div class="image ratio bg-primary bg-cover" style="background-image:url({image});"></div><h3 class="title pt1 m0 bold">{title}</h3></a></div>',
      });`}}
    />

  </IndexContainer>;
}