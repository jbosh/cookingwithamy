#!/usr/bin/env bash

usage() {
  cat << EOF
${BASH_SOURCE[0]} [options] <source> <destination>
Options:
  -q      Set quality of output (1-100, default = 80).
  -?      Print this help.
EOF
}
quality="80"

if (( $# == 0 )); then
    usage
    exit 1
fi

source=""
destination=""
while (( $# )); do
    case "$1" in
        "-q")
            if [[ "$2" == "" ]]; then
                echo "$1 requires argument."
                exit 1
            fi
            
            if (( $2 < 1 )) || (( $2 > 100 )); then
                echo "$1 argument $2 was out of range 1-100."
                exit 1
            fi
            quality="$2"
            shift
            ;;
        
        "-?")
            usage
            exit 0
            ;;
        
        "")
            ;;
        
        *)            
            if [[ "$source" == "" ]]; then
                if [[ ! -f "$1" ]]; then
                    echo "Could not find '$1'"
                    exit 1
                fi

                source="$1"
            elif [[ "$destination" == "" ]]; then
                destination="$1"
            else
                echo "Unknown option or too many paths $1";
                usage
                exit 1
            fi
            ;;
    esac
    
    shift
done

if [[ "$source" == "" ]]; then
    echo "No source."
    usage
    exit 1
fi

if [[ "$destination" == "" ]]; then
    echo "No destination."
    usage
    exit 1
fi

if [[ ! -f "$source" ]]; then
    echo "Could not find $source."
    exit 1
fi

destination="${destination%.*}.jpg"
destinationMin="${destination%.*}.min.jpg"
thumbDestination="thumbs/${destination%.*}.jpg"
thumbDestinationMin="thumbs/${destination%.*}.min.jpg"

echo "$source --> $destination"
magick "$source" -quality 100 "$destination"
magick "$source" -quality 100 -resize "300x300^" "$thumbDestination"

mozjpeg -quality "$quality" "$destination" > "$destinationMin"
mozjpeg -quality "$quality" "$thumbDestination" > "$thumbDestinationMin"
