---

title: Pad See Yew
image: pad-see-yew.jpg
imageCredit: https://www.tablefortwoblog.com/pad-see-ew-recipe/
tags: wok

prepTime: 10 minutes
cookTime: 15 minutes
totalTime: 40 minutes
makes: 5 servings

ingredients:
- Thick rice or udon noodle (3 packets frozen)
- "3 tbsp high heat oil *Recommended: sesame*"
- 2 cloves garlic cloves, very finely chopped
- ½ head of cabbage
- 3 chicken breasts
- 1 large egg (optional)
- 1 bunch broccoli
- 1½ cup chopped carrots
- ½ cup onion
- heading: Sauce
- 2 tbsp dark soy sauce
- 2 tbsp oyster sauce
- 2 tsp soy sauce (all purpose or light)
- 2 tsp white vinegar (or rice vinegar)
- 2 tsp sugar
- 2 tbsp water


directions:
- Dice every vegetable.
- Put all the sauce ingredients in a bowl and mix.
- Start boiling water, when it is ready, add noodles.
- Start heating wok on high heat. The hotter the better.
- Add high heat oil to wok and wait for it to heat.
- Start cooking carrots.
- When carrots start to soften, add the broccoli. The carrots will continue to cook.
- When broccoli starts to soften, add onions.
- When onions start to become semi-transparent, add chicken and cabbage.
- When cabbage starts to soften, add garlic, egg, and sauce.
- Add udon when everything starts to taste right.
---
