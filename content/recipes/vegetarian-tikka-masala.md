---

title: Vegetarian Tikka Masala
image: vegetarian-tikka-masala.jpg
imageCredit: https://www.twopeasandtheirpod.com/vegetarian-tikka-masala/

tags: vegetarian, Indian, slow cook, veggie

ingredients:
- 1 large yellow onion (½ goes in sauce in blitzed sauce, ½ is finely chopped)
- 2 cloves of garlic
- 1 inch of ginger, peeled
- 10 sprigs of fresh cilantro (plus more for serving)
- 2 tablespoons of olive oil, divided
- 1 teaspoon of cumin
- 1 teaspoon of turmeric
- ½ teaspoon of ground cinnamon
- 1 teaspoon of paprika
- 1 teaspoon of salt (optional)
- 3 teaspoons of garam masala
- 400g of canned plum tomatoes, not drained
- 1 15 ounce can of tomato sauce
- 1 small container of vegan yogurt, roughly 5 ounces
- 1 15 oz can of full fat coconut cream or milk
- ½ head of cauliflower, chopped into bite sized pieces
- 1 large carrot, peeled and finely chopped
- 1 15 oz can of chickpeas, drained and rinsed
- 1 cup of frozen peas
- 3oz tomato paste (half of 6oz can)


directions:
- In a food processor, combine half of the onion, garlic, ginger, cilantro stalks, and 1 tablespoon of olive oil. Blend until fully combined, about 1 minute. Set aside.
- In a large skillet, heat the remaining olive oil over medium heat. Add in the chopped onion and spices, stir to combine and cook gently for 10 minutes.
- Into the skillet, add the plum tomatoes (with the liquid from the can), tomato sauce, tomato paste, yogurt, HALF of the can of coconut milk and the blended onion and cilantro sauce. Cook for an additional 5 minutes over medium heat.
- Add in the cauliflower and carrots and bring to a simmer. Simmer for 20 minutes.
- Once the veggies are tender, add in chickpeas, frozen peas, and the remaining coconut milk. Stir to combine and let cook for 5 more minutes or until peas are fully heated through.

prepTime: 20 minutes
cookTime: 40 minutes
totalTime: 35 minutes
makes: 8 servings (with rice)
  
---