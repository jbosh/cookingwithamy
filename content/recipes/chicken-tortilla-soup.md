---

title: Chicken Tortilla Soup
image: chicken-tortilla-soup.jpg
imageCredit: https://natashaskitchen.com/chicken-tortilla-soup-recipe/

tags: soup

ingredients:
- 5 cups chicken stock or broth
- |
  14½ ounces (1 can) diced tomatoes undrained
  
  *Alternative:* You can also use 1-2 pounds of diced fresh tomatoes. Need to reduce the stock to 3½ cups if doing it this way.
- 1 medium onion, diced
- 4 cloves garlic finely minced
- 2 teaspoons chili powder
- 2 teaspoons cumin
- 1 teaspoon paprika
- 2 tsp of smoked paprika
- ¼ tsp of cayenne pepper
- 1 teaspoon coriander (ground)
- salt and pepper to taste
- 1½ pounds boneless skinless chicken breast
- 14½ ounces (1 can) black beans, drained and rinsed
- 2 cups frozen corn kernels
- 1 tablespoon lime juice
- ¼ cup chopped cilantro (optional)
- 1-3 jalapenos
- Add 3-4 roasted Pueblo/hatch chiles, rough chopped and seeded

- Two medium ears of fresh corn worked great, just cut the kernels off after steaming
- "*Optional toppings:* tortilla chips, diced avocado, shredded cheese, sour cream, or guacamole"



directions:
- Add the chicken stock, onions, tomatoes, and garlic to the pot. 
- Place the chicken breasts on top, and sprinkle the spices on top of that.
- Last, add the black beans, jalapenos, chilies, and corn on top, and do not stir.
- Place the lid on, lock it, and cook the mixture on manual high pressure for 5 minutes.
- Let the pressure release naturally for about 10 minutes (do not use the quick lever for 10 minutes). Then quick release the rest, using the release lever.
- When the pot has depressurized, remove the chicken and shred it with two forks.
- Return the chicken to the pot, and add lime juice and cilantro for additional flavor.



prepTime: 30 minutes
cookTime: 25 minutes
totalTime: 55 minutes
makes: 8 servings (with rice)
  
---

A classic Mexican soup for a cold day.