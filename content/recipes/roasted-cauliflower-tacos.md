---

title: Roasted Cauliflower Tacos
image: roasted-cauliflower-tacos.jpg
imageCredit: https://www.eatingbirdfood.com/cauliflower-tacos/
tags: Mexican
categories:
- Main

prepTime: 15 minutes
cookTime: 35 minutes
totalTime: 50 minutes
makes: 8 servings

ingredients:
- heading: Cauliflower
- 1 large head of cauliflower, sliced into bite-sized florets
- 2 to 3 tablespoons olive oil
- Salt and freshly ground black pepper
- heading: Seasoned Lentils
- 1 tablespoon olive oil
- 1 cup chopped yellow or white onion
- 2 large garlic cloves, pressed or minced
- 2 tablespoons tomato paste
- ½ teaspoon ground cumin
- ½ teaspoon chili powder
- ¾ cup brown lentils, picked over for debris and rinsed
- 2 cups broth
- heading: Chipotle sauce
- ⅓ cup mayonnaise
- 2 tablespoons lime juice
- 2 to 3 tablespoons adobo sauce (from a can of chipotle peppers) or chipotle hot sauce to taste
- Salt and freshly ground black pepper, to taste
- Everything else
- 8 small, round corn tortillas
- ½ cup packed fresh cilantro leaves *optional - use fresh spinach instead*


directions:
- "To roast the cauliflower: Preheat the oven to 425 degrees Fahrenheit. Toss cauliflower florets with enough olive oil to cover them in a light, even layer of oil. Season with salt and pepper and arrange the florets in a single layer on a large, rimmed baking sheet. Roast for 30 to 35 minutes, tossing halfway, until the florets are deeply golden on the edges."
- Warm the olive oil in a medium-sized pot over medium heat. Sauté the onion and garlic with a dash of salt for about 5 minutes, until the onions are softened and turning translucent. Add the tomato paste, cumin and chili powder and sauté for another minute, stirring constantly. Add the lentils and the vegetable broth or water. Raise heat and bring the mixture to a gentle simmer. Cook, uncovered, for 20 minutes to 45 minutes, until the lentils are tender and cooked through. Reduce heat as necessary to maintain a gentle simmer, and add more broth or water if the liquid evaporates before the lentils are done. Once the lentils are done cooking, drain off any excess liquid, then cover and set aside.
- To prepare the chipotle sauce, just whisk together the ingredients and set aside (if you have no choice but to use whole chipotle peppers from the can, use a blender to purée it all).
- Warm tortillas individually in a pan over medium heat. Stack the warm tortillas and cover them with a tea towel if you won’t be serving the tacos immediately.
- Once all of your components are ready, you can assemble your tacos! Top each tortilla with the lentil mixture, cauliflower, a drizzle of chipotle sauce and a generous sprinkle of chopped cilantro.

---
