---

title: Applesauce
image: applesauce.jpg
imageCredit: https://www.inspiredtaste.net/51372/applesauce/
tags: side, instant pot
categories:
- Side

prepTime: 5 minutes
cookTime: 15 minutes
totalTime: 20 minutes
makes: 8 servings

ingredients:
- 8 large Honeycrisp apples, peeled and cored
- 1 cup water
- 1 Tablespoon fresh lemon juice (from about 1/2 a lemon)
- 1/2 teaspoon ground cinnamon

directions:
- Peel and core the apples.
- Add everything to instant pot and stir to combine.
- Secure lid and cook on manual (high pressure) for 8 minutes.
- Do a controlled quick release or allow the pressure to naturally release.
- Mash the apple mixture a few times with a potato masher if desired. (If you like a smoother texture you could pulse it in the blender.) 
- Transfer contents to a bowl and allow to cool completely. Stir in more cinnamon to taste.

storage:
  store: Store in the fridge for up to 7-10 days.
---

Delicious homemade applesauce made quick in the instant pot. With no added sugar, this is a reasonably healthy recipe.