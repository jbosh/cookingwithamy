---

title: Zuppa Toscana
image: zuppa-toscana.jpg
imageCredit: https://momsdinner.net/instant-pot-sausage-kale-potato-soup/
tags: soup

categories:
- Dinner

ingredients:
- 1 lb Pork Sausage *mild, sweet, hot, or even turkey sausage*
- 1 tablespoon oil
- 3 garlic cloves minced
- 1 yellow onion chopped
- 4 tablespoons Italian seasoning
- 4 cups russet potatoes peeled, halved lengthwise, then sliced ¼ – ½ inch thick
- 6 cups chicken stock
- 4-6 cups fresh kale chopped, hard stem pieces removed
- 1 cup heavy whipping cream
- salt to taste
- pepper and red pepper flakes to taste

directions:
- Set your Instant Pot to saute mode.
- Add the oil and sausage, cook while crumbling the sausage, until it is cooked thru, about 5 minutes.
- Add the onions and garlic, saute an additional 5 minutes, stirring often.
- Turn off saute mode.
- Add the Italian seasoning, chicken broth, some salt, and potatoes to the sausage mixture.
- Put the lid on and lock in place. Turn the pressure valve to seal. 
- Set the pot to Soup/Broth OR Pressure mode (either will work). Set the timer for 6 minutes. 
- Walk away and let it cook!!
- Turn the pot off after the timer beeps and do a quick release of the pressure.
- Add the kale and cream. The heat of the soup will wilt the kale and warm the cream.

prepTime: 1 minute
cookTime: 10 minutes
totalTime: 11 minutes
makes: 2 servings
  
storage:
  store: Refrigerate for up to 5 days.
  reheat: Microwave works great for reheating this one.
---

A delicious Instant Zuppa Toscana that is full of sausage, kale, onions, and potatoes, with a light cream broth for richness.