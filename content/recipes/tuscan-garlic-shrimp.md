---

title: Tuscan Garlic Shrimp
image: tuscan-garlic-shrimp.jpg
imageCredit: https://www.paleorunningmomma.com/garlic-tuscan-shrimp-paleo-whole30/
tags: paleo, garlic, shrimp

categories:
- Dinner

ingredients:
- 1 lb medium-large shrimp uncooked, peeled, with tail on or off*
- Sea salt and black pepper
- 2 Tbsp ghee or avocado oil or coconut oil
- 1 small onion chopped
- 6 cloves garlic minced
- 1 Tbsp tapioca flour
- ½ cup chicken bone broth (or dry white wine if not Whole30}
- 1 cup coconut milk full fat
- ½ Tbsp mustard any kind
- 1½ Tbsp nutritional yeast optional, for flavor
- 1½ tsp Italian seasoning blend
- ¼ tsp sea salt or to taste
- ⅛ tsp black pepper or to taste
- ⅓ cup sun dried tomatoes roughly chopped**
- 2 cups fresh baby spinach
- 8oz grape tomatoes (optional)
- Chopped Fresh Parsley for garnish 

directions:
- heading: Shrimp
- Season the shrimp with sea salt and black pepper.
- Add the ghee or oil to a large skillet over medium high heat.
- Add the shrimp in a single layer and cook about two minutes on each side, until just cooked through.
- Set the shrimp aside on a plate and lower the heat to medium.

- heading: Sauce
- Add the onion to the skillet and cook for about a minute, then add the garlic and cook until fragrant, about 45-60 seconds.
- Whisk in the tapioca or arrowroot, the add the broth and coconut milk, scraping the bottom of the skillet.
- Stir to combine, then whisk in the mustard, nutritional yeast, and Italian seasoning.
- Cook and stir over medium heat until it starts to thicken, then taste and season with sea salt and pepper as desired.
- Add the sun-dried tomatoes and cook until softened, then add in the spinach to wilt.

- heading: Combine
- Return the shrimp to the skillet and stir, then sprinkle all over with parsley. 
- Serve over sautéed cauliflower rice or zucchini noodles to keep it low carb.

tips:
- Add some spaghetti noodles if you aren't feeling paleo.

prepTime: 1 minute
cookTime: 10 minutes
totalTime: 11 minutes
makes: 2 servings

storage:
  store: Refrigerate in an air tight container for up to 3 days.
  reheat: Toss a little butter in a pan on low heat and wait for shrimp to become warm.
