---

title: Butter Chicken
image: butter-chicken.jpg
imageCredit: https://www.kitchensanctuary.com/butter-chicken/

tags: Indian, slow cook, instant pot

ingredients:
- 3 tbsp butter
- 8 cloves garlic minced
- 2 tsp fresh ginger minced (or paste)
- 1 cup tomato puree
- 2 tbsp tomato paste
- 3 tsp garam masala
- 1 tbsp coriander ground
- 1 tsp cumin ground
- 1 tbsp smoked paprika
- 1 tsp turmeric
- 1 tsp salt
- 2 lbs chicken thighs boneless and skinless, cut into pieces
- 1 cup water (until it covers chicken) 
- 1 cup whipping cream (optional)
- 2 tbsp parsley chopped


directions:
- Turn your Instant Pot to the saute setting. 
- Add the butter and cook until the butter has melted. Add the garlic and ginger and saute for another minute or until the garlic becomes aromatic. Do not cook too long because you don't want to burn it.
- Add the tomato puree and tomato paste to the Instant Pot and stir. Add the garam masala, coriander, cumin, paprika, turmeric and salt to the Instant Pot, stir and cook for about 3 to 5 minutes. 
- Add the chicken thighs, water and stir everything together. There should be enough liquid in the pot to cover the chicken, so add more water if needed.
- Close the lid (follow the manufacturer's guide for instructions on how to close the instant pot lid). Set the Instant Pot to the Poultry setting and set the timer to 5 minutes.
- Once the Instant Pot cycle is complete, wait until the natural release cycle is complete, should take about 10 minutes. 
- Switch the Instant Pot to the saute setting (do not put the lid on). Add the whipping cream and simmer for 5 minutes, stirring occasionally. The sauce should thicken and reduce a bit. You don't want it to reduce too much. 
- Turn off the Instant Pot and garnish with fresh parsley. Serve warm over rice and with naan.


tips:
- Leaving the dish in the fridge overnight will help the spices set up, reheating the next day is terrific.


prepTime: 30 minutes
cookTime: 25 minutes
totalTime: 55 minutes
makes: 8 servings (with rice)
  
---

A classic Indian favorite.