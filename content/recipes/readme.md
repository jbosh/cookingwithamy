---


# Title of the recipe.
title: Readme Recipe

# Image located in /images. It can be any image supported (jpg, png, svg).
image: placeholder.svg

# Image credit (if needed).
imageCredit: https://example.com

# Display a warning that this recipe has not been tried yet.
experimental: true

# Recipe is a draft and shouldn't be output. The post will still be output, it just won't show up in search.
draft: true

# List of tags that can be searched on but don't appear on pages.
tags: readme, noodles, dinner

# List of categories that this dish has. It's usually not great to apply multiple.
categories:
- Dinner

# List of ingredients.
# Each individual item can be full markdown.
# Elements leading with `heading: ` will be converted to a sub-heading inside of ingredients.
ingredients:
- heading: The Sauce
- 4 tablespoons of water
- 2 teaspoons of salt
- heading: The Pasta
- 4 cups noodles
- Salt and pepper to taste

# List of directions.
# Each individual item can be full markdown.
# Elements leading with `heading: ` will be converted to a sub-heading inside of directions.
directions:
- heading: The Sauce
- On the stove top, heat a pot over high heat.
- When it comes to a boil, put in pasta.
- heading: The Pasta
- Cook for 10 minutes.

# List of tips to display at the top of the recipe. Each individual item is markdown.
tips:
- It's important to stir the pasta or it will cook *inconsistently*.

prepTime: 1 minute
cookTime: 10 minutes
totalTime: 11 minutes
makes: 2 servings

storage:
  store: Refrigerate for up to 1 week.
  reheat: Put in microwave after splashing with water for a couple minutes.
  freeze: |
    Frozen pasta wouldn't be that good.
    > Freezing pasta would be crazy.
    >                  - My mom.
  
---

Markdown section where you can place `notes` and other early elements before the recipe.