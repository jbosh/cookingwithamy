---

title: Corn Summer Quinoa Salad 
image: corn-summer-quinoa-salad.jpg
imageCredit: https://cookingwithcocktailrings.com
tags: dinner, salad, corn

categories:
- Dinner

ingredients:
- heading: Quinoa
- 2 cups chicken broth
- 1 cup uncooked white quinoa
- heading: Honey lemon vinaigrette
- 1 tablespoon lemon zest
- 2 tablespoons freshly squeezed lemon juice
- 3 tablespoons honey
- ¼ cup extra-virgin olive oil
- 1 clove garlic, minced
- ½ teaspoon salt
- ¼ teaspoon ground pepper
- heading: Vegetables
- 2 tablespoons extra-virgin olive oil
- 1 medium zucchini, cut in half lengthwise and sliced into half moons
- 5 ears corn, cut off the cob
- 2 cloves garlic, minced
- Kosher salt, as needed
- Freshly ground black pepper, as needed
- 1 medium Roma tomato, cored and diced
- 2 green onions, sliced
- ¼ cup roughly chopped flat-leaf parsley
- 1 tablespoon fresh dill
- ¾ cup crumbled feta cheese

directions:
- heading: Quinoa
- In a medium saucepan over medium heat bring the chicken broth to a boil. Add the quinoa, and cook covered on medium-low heat until the broth is absorbed and the quinoa is tender, about 25 minutes.
- Fluff the quinoa with a fork. Cover and set aside.
- heading: Honey Lemon dressing
- Combine the lemon zest, lemon juice, honey, olive oil, minced garlic, salt and pepper in a small bowl. Whisk to combine and set aside.
- heading: Vegetables
- Heat the remaining olive oil in a large skillet over medium-high heat. Add the zucchini sauté until tender, about 4 minutes.
- Add the corn and the garlic and continue to cook until the corn kernels soften, about an additional 3 minutes. Remove from heat and season with salt and pepper.
- In a large bowl combine the quinoa, zucchini and corn mixture, tomato, and green onion. Add the vinaigrette stirring to coat all the ingredients evenly. Add half of the feta cheese and herbs, stirring to combine once more.
- heading: For serving
- To serve, top with the remaining feta cheese. Season to taste with salt and pepper and serve at room temperature or refrigerate until chilled and serve. 

tips:
- Grilling the corn without the husks gives a fun charcoal flavor.
- Corn is what gives this dish its crunch. Don't be shy with it.
- Blistering whole tomatoes is an excellent choice.

prepTime: 20 minutes
cookTime: 20 minutes
totalTime: 40 minutes
makes: 4 servings

  
---

This summer quinoa salad recipe is packed with seasonal ingredients like zucchini, corn, tomatoes in this vegetarian dish.