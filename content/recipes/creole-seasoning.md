---


title: Creole Seasoning
image: creole-seasoning.jpg
imageCredit: https://www.daringgourmet.com/creole-seasoning-recipe/
tags: spice

categories:
- Spice

ingredients:
- 3 tablespoons paprika *smoked paprika also works*
- 2 tablespoons fine kosher salt
- 2 tablespoons garlic powder
- 1 tablespoon ground black pepper
- 1 tablespoon ground white pepper
- 1 tablespoons onion powder
- 1 tablespoon dried oregano
- ½ tablespoon cayenne
- ½ tablespoon dried thyme


directions:
- Mix all of the ingredients together in a bowl.

tips:
- If you like it hot, increase the cayenne to 1 tbsp.

prepTime: 1 minute
cookTime: 10 minutes
totalTime: 11 minutes
makes: 2 servings

storage:
  store: Store in a recycled spice container for up to 1 year.
  
---

A hot seasoning to add some southern flare to any dish.