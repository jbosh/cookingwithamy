---


title: Instant Pot Pulled Pork
image: instant-pot-pulled-pork.jpg
imageCredit: https://www.delish.com/uk/cooking/recipes/a29185240/slow-cooker-pulled-pork-recipe/
tags: readme, noodles, dinner

categories:
- Dinner

ingredients:
- 4 lb pork butt roast, boneless or bone-in
- 3 tbsp light brown sugar
- 2 tsp salt
- 1 tsp ground mustard
- 1 tsp black pepper
- 1 tsp onion powder
- 1 tsp paprika
- ½ tsp garlic powder
- ¼ tsp cayenne pepper
- 1½ cups chicken broth
- 1 tbsp Worcestershire sauce
- 1 tsp liquid smoke
- 2 tbsp olive oil
- ½ - 1 cup of your favorite BBQ sauce


directions:
- Trim fat from pork roast and cut into 4 same size chunks.
- Add all dry ingredients to a large bowl and whisk together.
- Add pork roast chunks to rub mix and coat well.
- Add 2 tbsp olive oil to a pan at high heat.
- Once the pan is hot, add 2 pieces of the pork. Sear on each side for about 2 minutes each. Remove pork and set aside on a plate. Repeat with remaining pork pieces.
- Add chicken broth, Worcestershire, and liquid smoke to the instant pot.
- Place pork chunks directly into the liquid in the pot, spacing out as best possible. Secure lid and make sure vent is set to sealing.
- Pressure cook/manual on high pressure for 60 minutes.
- Release pressure naturally for 20 minutes or until pin drops.
- Remove pork to a large bowl and shred with 2 forks.
- Add your favorite BBQ sauce as desired or use the juice from the Instant Pot.

prepTime: 10 minute
cookTime: 60 minutes
totalTime: 1 hour 10 minutes
makes: 6-8 servings
  
---

This was originally known as the best damn instant pot pulled pork.