---

title: Cheesy Lasagna
image: cheesy-lasagna.jpg
imageCredit: https://www.inspiredtaste.net/53854/cheese-lasagna/

tags: pasta, dinner, Italian, cast iron

ingredients:
- 2 tablespoons olive oil
- 1 cup diced onion (about ½ medium onion)
- 2 medium cloves garlic, diced
- 1 cup diced zucchini (about 1 small)
- 1 yellow squash
- 1 cup baby spinach leaves
- 3 cups Italian tomato sauce or other tomato-based pasta sauce of your choice
- Salt and pepper to taste
- 8 ounces lasagna noodles (about ½ pound), broken into 3 - 4 inch pieces
- 3 cups shredded mozzarella cheese (about 12 ounces)
- ¼ cup shredded Parmesan cheese
- 2 roma tomatoes (sliced)
- small amount of buffalo mozzarella

directions:
- Heat oven to 375°. Place a 12-inch or larger cast-iron skillet on the stove top over medium heat. Heat olive oil and add onions, stirring frequently, until they begin to soften. Add the garlic, sauteing for another minute. Add the zucchini and mushrooms and saute for 3 - 4 minutes until the zucchini is tender and the mushrooms soften.
- Pour Italian tomato sauce over veggies and combine, stirring over medium heat until warmed through. Stir in baby spinach leaves and cook for an additional minute. Taste sauce and add a little salt and pepper if desired.
- Remove the pan from heat.
- Remove all but 1 cup of the sauce from the pan. Spread the remaining sauce evenly in the bottom of the pan and cover with a layer of noodles. No need to place them perfectly evenly, just place them here and there in a single layer until most of the sauce is covered by the pasta.
- Add ⅓ of roma tomatoes onto the noodles. Add ⅓ of the remaining sauce, spreading evenly with the back of a spoon, and then sprinkle ⅓ of the mozzarella. Add another layer of noodles. Repeat with another layer tomatoes, sauce, mozzarella, and noodles, then spoon the remaining sauce on the top, sprinkle with the remaining mozzarella cheese, and top with the Parmesan cheese and buffalo mozzarella.
- Place the skillet in the oven and bake for about 30 minutes, until the cheese is bubbly and the pasta is cooked through.
- Remove from the oven and lit cool for about 5 minutes, then serve.


tips:
- Simmer the sauce (without zucchini) for as long as possible. It thickens up and tastes way better. Add some fresh basil if possible.
- Undercook the zucchini for a crunch.

prepTime: 30 minutes
cookTime: 25 minutes
totalTime: 55 minutes
makes: 4 servings
  
---

Vegetarian lasagna to keep the whole family guessing where the meat went.