---

title: Pesto Chicken Gnocchi
image: pesto-chicken-gnocchi.jpg
imageCredit: https://www.littlehomeinthemaking.com/pesto-chicken-gnocchi/
tags: noodles, dinner

categories:
- Dinner

ingredients:
- 2 boneless, skinless chicken breasts (cut in half widthwise)
- 4 tablespoons extra virgin olive oil, divided
- ¾ teaspoon paprika
- ½ teaspoon garlic powder
- ½ teaspoon onion powder
- ½ teaspoon kosher salt
- ⅛ teaspoon ground black pepper
- 1 cup cherry tomatoes, halved
- ¾ cup prepared basil pesto
- 1 (500g/16oz) package gnocchi
- Boiling salted water

directions:
- Add 2 tablespoons of the olive oil to a medium size bowl. Add in the paprika, garlic powder, onion powder, kosher salt, and black pepper. Whisk to combine.
- Cut your chicken breast into halves widthwise, making 4 thinner pieces in total (also know as cutlets).
- Add the chicken cutlets to the bowl, and toss to coat the chicken breasts with the oil and seasoning mixture.
- Heat a heavy bottomed skillet over medium heat. Once the skillet is hot, add the remaining 2 tablespoons of olive oil to the pan and swirl to coat.
- Once the olive oil is shiny, add the seasoned chicken in a single layer. Cook for 5 minutes, then turn to cook the other side.
- Cook your chicken for an additional 4-5 minutes, or until the thickest part of each chicken piece reaches a minimum internal temperature of 165ºF.
- Remove the cooked chicken breasts from the skillet and place them on a plate. Set aside.
- Add the cherry tomatoes to the hot skillet, and toss to coat the tomatoes. Cook for 1-2 minutes, stirring or shaking the pan often.
- Add the tomatoes to a large bowl and set aside.
- Cut your cooked chicken breasts into slices or bite-sized pieces and add them to the bowl with the tomatoes. Add in ¼ cup of the pesto and stir to combine.
- Bring a large pot of salted water to a rolling boil.
- Add the gnocchi to the boiling water, and cook for 3-5 minutes, removing the gnocchi as they float to the surface of the water. Place in a collander set over the sink. Gnocchi float as they cook through, so you know they are done when they are floating.
- Add the cooked gnocchi to the bowl with the chicken and cherry tomato halves. Add in the remaining ½ cup of pesto, and toss to combine.
- Serve immediately, garnished with fresh basil or grated parmesan cheese, if desired. 

prepTime: 5 minute
cookTime: 15 minutes
totalTime: 20 minutes
makes: 4 servings