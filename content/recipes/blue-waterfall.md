---

title: Blue Waterfall
image: blue-waterfall.jpg
imageCredit: https://www.bakingbeauty.net/blue-lagoon-cocktail/
tags: drink
categories:
- Drink

prepTime: 1 minute
totalTime: 1 minute
makes: 1 serving

ingredients:
- ¾ gin
- ¾ triple sec
- ¾ lemom
- ¾ gin creme de violette
- 1 egg white


directions:
- Put all the ingredients in a mixer.
- Shake, don't stir.

---

"This car is parking itself."