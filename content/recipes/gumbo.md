---

title: Gumbo
image: gumbo.jpg
imageCredit: https://thealmondeater.com/shrimp-and-sausage-gumbo/

tags: instant pot, whole 30

ingredients:
- 24 ounces sea bass filets patted dry and cut into 2-inch chunks (tilapia works too)
- 3 tablespoons avocado oil (or ghee)
- 3 tablespoons cajun seasoning or creole seasoning
- 2 yellow onions diced
- 2 bell peppers diced
- 1 celery ribs diced
- 28 ounces diced tomatoes
- ¼ cup tomato paste
- 3 bay leaves
- 1½ cups bone broth
- 8 links of andouille sausage
- 2 pounds medium to large raw shrimp deveined
- Sea salt to taste
- Black pepper to taste



directions:
- Start browning the onions, peppers, and celery with avocado oil. Cook for approximately 5 minutes.
- Add spices to mixture and cook for 2 minutes until fragrant.
- Add sausage, tomatoes, broth, bay leaves, and previous mixture to the instant pot.
- Put lid on instant pot and cook for 5 minutes on high pressure.
- When cooking has completed, set pot to venting.
- While venting, put sea bass into pan and fry until brown. This takes approximately 5 minutes.
- Open instant pot and put shrimp in while on "Keep warm" until shrimp is cooked.
- Serve on top of sea bass and rice.


tips:
- Serve with tiny noodles if you're not feeling rice.


prepTime: 15 minutes
cookTime: 35 minutes
totalTime: 55 minutes
makes: 8 servings (with rice)
  
---

A whole 30 compliant cajun favorite.