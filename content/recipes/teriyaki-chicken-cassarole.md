---

title: Teriyaki Chicken Casserole
image: teriyaki-chicken-casserole.jpg
imageCredit: https://homemadehooplah.com/teriyaki-chicken-casserole/
tags: Southeast Asian

categories:
- Dinner

ingredients:
- ¾ cup low-sodium soy sauce
- ½ cup water
- ¼ cup brown sugar (I used ⅓)
- ½ teaspoon ground ginger (I used fresh, minced)
- ½ teaspoon minced garlic
- 2 Tablespoons cornstarch + 2 Tablespoons water
- 1 pound boneless skinless chicken breasts
- 1 bag (12 ounces) stir-fry vegetables *(broccoli, carrots, snow peas, green onions, bell peppers, etc...)*
- 3 cups cooked brown or white rice


directions:
- Preheat oven to 350°F. Spray a 9x13-inch baking pan with non-stick spray.
- Combine soy sauce, ½ cup water, brown sugar, ginger, and garlic in a small saucepan and cover.
- Bring to a boil over medium heat.
- Remove lid and cook for one minute once boiling.
- Meanwhile, stir together the corn starch and 2 tablespoons of water in a separate dish until smooth. **Important!**
- Once sauce is boiling, add mixture to the saucepan and stir to combine.
- Cook until the sauce starts to thicken then remove from heat.
- Place the chicken breasts in the prepared pan.
- Pour one cup of the sauce over top of chicken.
- Place chicken in oven and bake 35 minutes or until cooked through.
- Remove from oven and shred chicken in the dish using two forks.
- Meanwhile, pan fry the vegetables.
- Add the cooked vegetables and cooked rice to the dish with the chicken.
- Add most of the remaining sauce, reserving a bit to drizzle over the top when serving.
- Gently toss everything together in the dish until combined.
- Return to oven (or stove top with wok) and cook 15 minutes.
- Remove from oven and let stand 5 minutes before serving.
- Drizzle each serving with remaining sauce.

prepTime: 35 minute
cookTime: 15 minutes
totalTime: 50 minutes
makes: 4 servings

tips:
- Use a wok on high heat, not a casserole dish.

---

This Chicken Teriyaki Casserole is a full meal in one pan!