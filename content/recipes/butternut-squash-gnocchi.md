---

title: Butternut Squash Gnocchi
image: butternut-squash-gnocchi.jpg
imageCredit: https://www.wellplated.com/italian-sausage-butternut-squash-gnocchi/
tags: cast iron

prepTime: 15 minutes
cookTime: 25 minutes
totalTime: 40 minutes
makes: 4 servings

ingredients:
- 5 tablespoons unsalted butter divided
- 1 pound ground sweet Italian turkey sausage removed from casings if needed
- 1 small butternut squash about 1 1/4 pounds, *cut into 1/2-inch cubes (about 3 1/2 cups cubes total; if you have a little extra, use a very large skillet and add it all if you really love squash, or reserve the extra for another time)*
- 1 small yellow onion *diced, about 1 cup*
- 3 cloves garlic *minced*
- 1/2 teaspoon kosher salt *plus additional to taste*
- 1/4 teaspoon black pepper
- 1 teaspoon rubbed sage *or 1 tablespoon chopped fresh sage*
- 1/4 teaspoon ground nutmeg
- 1 1/2 to 2 cups low-sodium chicken broth
- 1 pound potato gnocchi*
- 5 cups loosely packed baby spinach *about 5 ounces or 5 generous handfuls*
- 1/4 cup freshly grated Parmesan *optional—we loved it both with and without*
- Pinch red pepper flakes *optional*

directions:
- Melt 1 tablespoon of butter in a very large skillet with a tight-fitting lid over medium high. Once the butter is melted, add the sausage. Cook, breaking apart the meat, until it is browned and cooked through, about 5 minutes. Transfer the sausage to a plate.
- Reduce the skillet heat to medium. Add the remaining 4 tablespoons butter. Once melted, add the squash and onion. Cook, stirring occasionally, until the squash is tender and golden, about 8 to 10 minutes. Add the garlic, salt, pepper, rubbed sage (if using fresh sage, wait to add it until the end), and nutmeg. Continue cooking until the garlic is softened and fragrant, about 1 additional minute.
- Add 1 1/2 cups chicken broth and gnocchi to the skillet. Increase the heat to medium high. Stir to coat the gnocchi, then cover the pan. Cook, covered, until the gnocchi is just tender, about 5 minutes. Uncover and stir in the sausage. Stir in the spinach a few handfuls at a time, allowing it to wilt. If you’d like a looser sauce, add a bit more chicken broth. Sprinkle with Parmesan and season to taste with additional salt, pepper, and red pepper flakes. Enjoy hot.

storage:
  store: Refrigerate leftover gnocchi in an airtight storage container for up to 4 days.
  reheat: Rewarm gently in the microwave or on the stovetop with a splash of extra chicken broth or water to keep it from drying out.
  freeze: Freeze leftovers in an airtight freezer-safe storage container for up to 3 months. Let thaw overnight in the refrigerator before reheating. 

---

One Skillet Butternut Squash Gnocchi with Italian Sausage and Spinach. Healthy, EASY recipe that cooks in just one pan! Pure comfort and so simple to make!