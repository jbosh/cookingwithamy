---

title: Readme Recipe
image: placeholder.svg
imageCredit: https://example.com
tags: readme, noodles, dinner

categories:
- Dinner

experimental: true
draft: true

ingredients:
- heading: The Sauce
- 4 tablespoons of water
- 2 teaspoons of salt
- heading: The Pasta
- 4 cups noodles
- Salt and pepper to taste

directions:
- heading: The Sauce
- On the stove top, heat a pot over high heat.
- When it comes to a boil, put in pasta.
- heading: The Pasta
- Cook for 10 minutes.

tips:
- It's important to stir the pasta or it will cook *inconsistently*.

prepTime: 1 minute
cookTime: 10 minutes
totalTime: 11 minutes
makes: 2 servings

storage:
  store: Refrigerate for up to 1 week.
  reheat: Put in microwave after splashing with water for a couple minutes.
  freeze: |
    Frozen pasta wouldn't be that good.
    > Freezing pasta would be crazy.
    >                  - My mom.
  
---

Markdown section where you can place `notes` and other early elements before the recipe.