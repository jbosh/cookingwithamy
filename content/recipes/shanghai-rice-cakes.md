---

title: Shanghai Rice Cakes
image: shanghai-rice-cakes.jpg
imageCredit: https://drivemehungry.com/stir-fried-shanghai-rice-cakes-chao-nian-gao/
tags: wok, Chinese

prepTime: 30 minutes
cookTime: 20 minutes
totalTime: 50 minutes
makes: 6 servings

ingredients:
- 1 lb chicken
- 12 oz rice cakes
- 1 cup carrots
- 2 cups broccoli
- 3 cups cabbage
- 3 scallions
- 2 clove garlic
- 2 tablespoon oil
- 2 tablespoon shaoxing wine
- 2 tablespoon dark soy sauce
- 2 tablespoon light soy sauce
- 1 tablespoon oyster sauce *optional*
- ¼ teaspoon ground white pepper
- 2 teaspoon salt
- ¼ teaspoon sugar

directions:
- Slice the chicken into thin slices and mix well in a bowl with 1 teaspoon each of cornstarch, soy sauce, and oil.
- Rinse the rice cakes in water and drain.
- Wash the cabbage and broccoli. Cut the cabbage into 1 inch slices.
- Clean and chop your scallions into 2-inch pieces.
- Mince your garlic.
- Set everything aside.
- Start boiling the rice cakes until they separate.
- Heat the wok over high heat (as hot as it goes) until smoking and add 1 tablespoon oil to coat the wok and sear the chicken.
- Add garlic, scallions, cabbage, broccoli, and carrots.
- Stir fry on high heat for a minute and add the shaoxing wine.
- Add the rice cakes and mix well, scooping up from the bottom of the wok for 30 seconds and then cover for one minute.
- Remove cover and add the soy sauces, oyster sauce, white pepper, salt, and sugar.
- Mix well and stir-fry until the rice cakes are cooked through but still chewy. Plate and serve family-style.

tips:
- You can use any sugar you want to create some interesting flavors. Some favorites we've had have been honey and bbq sauce.

storage:
  reheat: |
    - Put in the microwave after splashing some soy sauce.
    - You can also reheat on a wok on in a pan with some soy sauce.
  store: Store in the fridge for up to 7-10 days.
---

A classic Chinese recipe to make use of this unusual carbohydrate.