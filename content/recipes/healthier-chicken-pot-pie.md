---

title: Healthier Chicken Pot Pie
image: chicken-pot-pie.jpg
imageCredit: https://www.eatwell101.com/chicken-pot-pie-recipe
tags: dinner

categories:
- Dinner

ingredients:
- 3 lbs boneless skinless chicken breast *about 6 breasts*
- 1 tsp kosher salt & fresh cracked pepper *or "The Blend"*
- 2 8 oz packages of crescent rolls
- 2 tablespoon butter/oil
- 2 cups carrots diced
- 2 cups celery diced
- 2 cups white onion diced
- 1 tsp kosher salt
- 1 tsp garlic powder
- 1 tsp onion powder
- "1 tsp poultry seasoning *Alternatively: McCormicks with 1 tbsp bouillon*"
- 4½ tablespoons flour
- 3 cups reduced sodium chicken broth
- 1 cup whole milk
- 3 oz cream cheese
- pinch nutmeg
- 2 cups frozen peas


directions:
- Preheat the oven to 450°F.
- Season the chicken with the salt and pepper *or "The Blend"* on both sides.
- Place the chicken on the sheet pan and roast for 25 minutes or until fully cooked. *Approximately 13 minutes in a convection oven.*
- Place the chicken on a cutting board and chop into bite-sized pieces. Leave on the cutting board with the residual juices while you prepare the other pie ingredients.
- Sauté the carrot, celery, and onion (in oil or butter) for 5-6 minutes or until they get soft and start to brown.
- Add the salt, pepper, garlic powder, onion powder, and poultry seasoning. Stir to combine.
- Add olive oil spray to the pan with the vegetables and sprinkle flour on top.
- Stir the flour to combine with the vegetables. Cook for 1 minute.
- Add the chicken broth, stir to combine, and allow the mixture to come to a boil.
- Cook for 5 minutes or until the mixture thickens significantly. It should not be watery.
- Unroll the crescent rolls and bake them at 375°F until they are just starting to brown. *Approximately 7 min*.
- Line the bottom of a 9"x13" glass dish with ½ of the rolls and cook the other ½ on a cookie sheet.
- Stir the milk and cream cheese into the skillet, whisk to incorporate. The mixture should be thick in consistency, but not dry. If it's too dry, add a splash more stock. Add a pinch of nutmeg.
- Add the cooked chicken along with the juices, as well as the frozen peas. Stir to combine.
- Pour the chicken mixture over the partially cooked rolls in the 9"x13" dish and then cover with the other ½ of the rolls.
- Bake at 375°F until the top is golden brown. *Approximately 10 minutes*.

prepTime: 20 minute
cookTime: 50 minutes
totalTime: 70 minutes
makes: 2 servings
  
---

Chicken pot pie with a healthy twist. Sorta.