---

title: Chicken Parmesan
image: chicken-parmesan.jpg
imageCredit: https://www.kitchensanctuary.com/chicken-parmesan/

tags: pasta, dinner, Italian


ingredients:
- 3 chicken breasts
- Salt and pepper
- ½ cup italian breadcrumbs
- 1 egg, beaten
- 4 tablespoons butter
- 2 cups spaghetti sauce
- 3 slices mozzarella cheese (optional)
- 1 tablespoon grated parmesan (optional)
- Noodles


directions:
- Heat up the skillet on the stove, wait until it’s hot. Probably med-high.
- Pound chicken to flatten it. Get it really flat.
- Salt and pepper the chicken.
- "*Optional:* Let the chicken sit for a minute to absorb the flavors."
- Beat an egg in a big bowl and place the bread crumbs in another bowl.
- Dip the chicken in the egg then in the bread crumbs, really coat it.
- Put all the butter in a skillet. Wait for it to get slightly melty.
- Put all chicken in a skillet on the stove.
- Fry until chicken is close to done (15 minutes for thick breasts).
- Add spaghetti sauce to the pan.
- Place mozzarella on top of chicken.
- Sprinkle parmesan on top of that.
- Cover until the cheese is melted.


tips:
- Add whatever vegetables you want to the pan after adding sauce.
- It’s worthwhile to let the chicken absorb the salt/pepper for a minute before coating them in breadcrumbs.
- Definitely pound the chicken otherwise they don’t get flat enough and breadcrumbs get burny. It’s still good, just the presentation value is low.
- Use a dish with a full lid to melt the mozzarella better, or flip it an have it caramelize on the bottom of the pan.


prepTime: 20 minutes
cookTime: 15 minutes
totalTime: 35 minutes
makes: 3 servings
  
---

Vegetarian lasagna to keep the whole family guessing where the meat went.