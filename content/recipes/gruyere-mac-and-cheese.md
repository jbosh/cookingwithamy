---

title: Gruyere Mac and Cheese
image: gruyere-mac-and-cheese.jpg
imageCredit: https://www.williams-sonoma.com/recipe/gruyere-and-cheddar-mac-and-cheese.html
tags: dinner, pasta, noodles

categories:
- Dinner

ingredients:
- 1 lb. (500 g) dried short pasta, such as straccetti or campanelle
- 2 Tbs. olive oil
- 4 Tbs. (½ stick) (2 oz./60 g) unsalted butter
- ¼ cup (1½ oz./45 g) all-purpose flour
- 2 cups (16 fl. oz./500 ml) milk
- 1 cup (8 fl. oz./250 ml) half-and-half
- ¼ tsp. freshly ground nutmeg
- Salt and freshly ground pepper, to taste
- 1½ cups (6 oz./180 g) shredded Gruyère cheese
- 1½ cups (6 oz./180 g) shredded white cheddar cheese
- ¼ cup (1 oz./30 g) grated Parmesan cheese
- ¼ cup (1 oz./30 g) panko (Japanese bread crumbs)

directions:
- Preheat an oven to 375°F (180°C).
- Bring a large saucepan of salted water to a boil over high heat.
- Add the pasta and cook, stirring occasionally, until not quite al dente, about 2 minutes less than the package instructions.
- Drain and transfer to a large bowl.
- While the pasta is still warm, drizzle with the olive oil and stir well.
- Return the saucepan to medium-high heat and melt the butter.
- Add the flour and cook, stirring well, until the flour is thoroughly incorporated, 1 to 3 minutes.
- Whisk in the milk, half-and-half, nutmeg, and a generous pinch of salt and bring to a boil.
- Simmer, whisking frequently to smooth out any lumps, for 4 to 5 minutes.
- Remove from the heat.
- Add a pinch of pepper and two-thirds each of the Gruyère and cheddar. Stir until smooth.
- Pour the cheese sauce onto the macaroni and stir well.
- Transfer to a Le Creuset 2.75-quart shallow oven or large baking dish with a similar capacity and top with the remaining
  one-third Gruyère and cheddar and all of the Parmesan.
- Sprinkle evenly with the panko. Bake until the top is lightly browned and the sauce is bubbly, 25 to 30 minutes.

prepTime: 30 minute
cookTime: 50 minutes
totalTime: 1 hour 20 minutes
makes: 6 servings

---