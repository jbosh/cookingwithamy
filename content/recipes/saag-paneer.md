---

title: Saag Paneer
image: saag-paneer.jpg
imageCredit: https://healthynibblesandbits.com/easy-palak-paneer/
tags: Indian

categories:
- Dinner

ingredients:
- 1 pound fresh baby spinach *about 8 packed cups*
- 2 tablespoons ghee
- 8 ounces paneer, cut into 1-by-½-inch pieces
- ½ cup finely chopped yellow onion
- Kosher salt and black pepper
- 1 tablespoon freshly grated peeled ginger *from a 2-inch piece*
- 2 teaspoons grated garlic *about 3 cloves*
- 1 serrano chile, stemmed and minced
- ½ teaspoon ground coriander
- ¼ teaspoon ground cumin
- ½ cup heavy cream
- heading: For serving
- Naan bread
- Jasmine rice


directions:
- In a food processor, working in batches, pulse spinach until minced but not puréed. Do not pack the spinach too tightly or it won’t get evenly chopped. You should have about 3 packed cups of minced spinach.
- Heat 1 tablespoon ghee over medium heat in a large nonstick skillet.
- When it shimmers, add paneer and cook, turning occasionally, until golden all over, 5 to 7 minutes
- Using tongs or a fish spatula, transfer cheese to a plate, leaving as much ghee as possible in the skillet.
- Reduce heat to medium-low, add the remaining 1 tablespoon ghee and the onion, and season with salt and pepper.
- Cook, stirring occasionally, until softened but not browned, about 5 minutes.
- Add ginger, garlic and chile, and cook, stirring occasionally, until fragrant and well incorporated, 1 minute.
- Stir in coriander and cumin until well blended.
- Add minced spinach and ½ cup water, and increase the heat to medium.
- Season with salt and pepper and cook, stirring occasionally, until spinach is completely soft and most of the liquid is absorbed, about 8 minutes.
- Stir in heavy cream and paneer until well incorporated. Season with salt and pepper. Transfer to a serving bowl.


prepTime: 10 minute
cookTime: 15 minutes
totalTime: 35 minutes
makes: 4 servings
  
---

A healthy Indian dish.