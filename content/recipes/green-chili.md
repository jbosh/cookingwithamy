---

title: Green Chili
image: green-chili.jpg
imageCredit: https://www.thespruceeats.com/pork-green-chile-2216665

tags: slow cook, instant pot

ingredients:
- 4 pounds pork shoulder, cut into 1½ inch cubes (trim off as much fat as possible)
- 27 ounces mild fire roasted diced green peppers (one 28 ounce can, or 6 (4-ounce) cans)
- "16 ounce jar salsa or salsa verde. **Recommended:** King’s Chef Green Chile and Habanero"
- 2 teaspoons Diamond Crystal Kosher salt
- 1 tablespoon vegetable oil
- 1 large onion, diced
- ½ teaspoon Kosher salt
- 4 cloves garlic, crushed
- 1 tablespoon ground cumin
- 1 tablespoon ground coriander
- 1 tablespoon dried oregano (preferably Mexican oregano)
- 1 cup chicken stock (preferably homemade) or water
- Juice of 1 lime (for serving)
- Salt and pepper
- 1 cup of heavy cream or whole milk (optional)


directions:
- Mix the canned diced chiles and salsa in a medium bowl and set aside.
- heading: Brown the Pork
- Sprinkle the pork with the 2 teaspoons of kosher salt.
- Heat the oil in a frying pan over high heat until it shimmers.
- Brown the pork in two to three batches - put pork cubes in the frying pan without crowding, and brown each batch of pork on one side, about 4 minutes.
- Transfer the pork to the instant pot.
- heading: Saute the Aromatics and Toast the Spices
- There will be extra fat in the frying pan from the pork. This is going to soak that up.
- Add the onions to the frying pan and sprinkle with the ½ teaspoon of kosher salt.
- Saute the onions until softened and starting to brown around the edges, about 5 minutes, scraping often to release any browned pork bits from the bottom of the frying pan.
- Make a hole in the middle of the onions and add the garlic, cumin, coriander, and oregano.
- Cook until you smell the garlic and spices, about 1 minute, then stir into the onions.
- heading: Pressure Cook
- Stir the chicken stock, salsa verde, pork, and any pork juices in the bowl into the pot.
- Lock the lid and cook at high pressure for 30 minutes in an electric PC.
- Let the pressure come down naturally, about 20 minutes.

tips:
- Cut the onions really thin or caramelize them or they become overpowering.
- Add ½ tsp of smoked paprika for a little hint of smoky. It turns the chili red though.
- Stir in some lime juice when serving.


prepTime: 10 minutes
cookTime: 30 minutes
totalTime: 50 minutes
makes: 6 servings (with rice)
  
---

A hot dish for a cold day.