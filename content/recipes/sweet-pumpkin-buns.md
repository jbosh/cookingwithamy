---

title: Sweet Pumpkin Buns
image: sweet-pumpkin-buns.jpg
imageCredit: http://www.landsandflavors.com/sweet-pumpkin-buns/
tags: dessert

prepTime: 30 minutes
cookTime: 20 minutes
totalTime: 60 minutes *1:30 of rise time.*
makes: 8 servings

ingredients:
- ¾ cup milk
- ½ cup sugar (or sweetener of choice)
- 1 teaspoon yeast
- ¾ cup pumpkin purée
- ¼ cup oil (any neutral oil, I used a combination of coconut and sunflower)
- 2 teaspoons cinnamon
- ¼ teaspoon nutmeg
- ¼ teaspoon allspice
- ⅛ teaspoon clove
- 1 teaspoon pure vanilla extract
- ¼ teaspoon sea salt
- 3¼ cups flour
- 4 cinnamon sticks, broken in half

directions:
- In a small saucepan over medium heat, bring the nondairy milk and the sugar to a simmer. You just want to dissolve the sugar, so once it dissolves, remove from heat.
- Remove ¼ cup of this mixture and set it aside in a small bowl for later. Add the greater amount of the mixture to a bowl of a mixer and let it cool down until it's just warm.
- When this milk mixture has cooled enough, add to it the yeast, pumpkin purée, oil, cinnamon, nutmeg, allspice, cloves, vanilla and salt.
- Turn the mixer on low to incorporate the ingredients for around 1 minute.
- Now add the flour and mix until thoroughly combined. I like to use a dough hook attachment for this. Mix it on medium-high for around 3 minutes. *The dough may appear a bit tacky, no worries though. Just oil your hands lightly and form it into a ball then oil the inside of the bowl before throwing it back in.*
- Allow it to proof, covered and in a warm spot, for 1 hour.
- After 1 hour, lightly flour your work surface and turn out the dough onto it. Use a knife to cut the dough into 8 equal-sized pieces (cut it like a pizza).
- Each of these pieces is then kneaded lightly, then rolled into a ball. Hold each ball in one hand while you use a pair of kitchen scissors in the other to cut 8 slits all around.
- Put each of the sliced balls on a parchment-lined baking sheet and stick half of a cinnamon stick into the center of each one. Twist it slightly to secure it.
- Cover the trays of pumpkins with damp towels and let them rise again for 30 minutes in a warm location. Preheat the oven to 375°F.
- When they have risen, use a pastry brush to gently brush the surfaces of each pumpkin with that ¼ cup of remaining milk and sugar mixture (from Step 1 above). Don't worry if you don't use all of this glaze.
- Bake them for 18-22 minutes or until they are golden brown.
- Remove from oven and cool on a rack thoroughly before eating (they will taste gummy if you eat them just out of the oven).

storage:
  store: Store in an airtight container for up to 5 days at room temperature.

tips:
  - Serve with a little butter for optimal flavor.
---

These delightful Sweet Pumpkin Buns feature the comforting flavor of pumpkin along with warm spices and a hint of sweetness. A festive treat for the Fall holidays!
