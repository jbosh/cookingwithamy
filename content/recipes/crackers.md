---

title: Crackers
image: crackers.jpg
imageCredit: https://www.webstaurantstore.com/nabisco-ritz-32-count-sleeve-original-crackers-case/565014200.html

tags: breakfast, habenero

ingredients:
- 2 cups all-purpose flour
- 3 teaspoons baking powder
- 1 tablespoon sugar
- ½ teaspoon salt
- 6 tablespoons cold unsalted butter, cut into cubes
- 2 tablespoons vegetable oil
- ⅔ cup cold water
- 4 tablespoons unsalted butter, melted
- coarsely ground sea salt, for topping



directions:
- Preheat the oven to 400°F.
- Place flour, baking powder, sugar, ½ teaspoon salt in the bowl of a food processor and pulse to combine.
- Add butter and pulse to combine.
- Add vegetable oil and pulse to combine.
- With a food processor on low speed,slowly drizzle in the water until the dough begins to form a ball. Remove dough from the food processor and wrap in plastic wrap. Let rest for 10 minutes.
- On a lightly floured surface, roll out dough as thin as possible. **Important** make them as thin as you possibly can. I cannot stress this enough.
- Cut the dough into your desired shapes with a cookie cutter, or cut rectangles or squares with a knife or pizza cutter and arrange on a baking sheet lined with parchment paper.
- Gather scraps of dough and re-roll until all dough is used. (If you struggle to get the dough to roll thin enough, let it rest for another 10 minutes, then come back to it. The gluten may need to relax a bit.)
- Using a toothpick, prick several holes in each cracker. (Don't skip this step or you will not have nice, flat crackers.)
- Bake for 10 minutes, or until golden brown.
- Remove from the oven, brush the tops with melted butter, and immediately sprinkle with coarsely ground sea salt. (If you don't - sprinkle the salt on the melted butter right away, it won't stick.)
- Remove from baking sheets and allow to cool completely on a wire rack.


tips:
- Make sure they're thin. Make them as thin as you can and then make them even thinner.
- Brown them, they should smell a little bit burnt.

prepTime: 50 minutes
cookTime: 10 minutes
totalTime: 1 hour
makes: 100 crackers
  
---

Ritz crackers but you make them yourself! These are really hard to make right, so strap in.