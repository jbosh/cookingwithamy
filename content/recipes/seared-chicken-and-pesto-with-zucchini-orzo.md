---

title: Seared Chicken and Pesto with Zucchini Orzo
image: seared-chicken-and-pesto-with-zucchini-orzo.jpg
imageCredit: https://www.goodhousekeeping.com/food-recipes/a28611486/seared-chicken-with-pesto-zucchini-orzo-recipe/
tags: noodles

categories:
- Dinner

ingredients:
- 8 oz orzo
- 2 tbsp olive oil, divided
- 4 6-ounce boneless, skinless chicken breasts
- Kosher salt and pepper
- ¼ cup white wine vinegar
- 2 small zucchini (about 4 ounces each), very thinly sliced
- 2 small yellow squash (about 4 ounces each), very thinly sliced
- ½ cup store-bought pesto
- ¼ cup roasted almonds, chopped *optional*

directions:
- Prepare orzo per package directions. Drain well, then transfer to a large bowl; toss with 1 tablespoon oil.
- Meanwhile, heat remaining tablespoon oil in a large skillet on medium.
- Season chicken with ½ teaspoon each salt and pepper and cook until golden brown and cooked through, 5 to 7 minutes per side; transfer to plates. *OR use grill mates marinade and dip in breadcrumbs*
- Roast zucchini and yellow squash in air fryer for 5 min.
- In another large bowl, whisk together vinegar and ¼ teaspoon each salt and pepper. Add zucchini and squash and toss to combine. Let sit until orzo is done, at least 5 minutes.
- With a slotted spoon, transfer vegetables to bowl with orzo, leaving vinegar in bowl, and toss to combine.
- Whisk pesto into vinegar, then drizzle over orzo and sprinkle with almonds. Serve with chicken.

prepTime: 15 minute
cookTime: 10 minutes
totalTime: 25 minutes
makes: 4 servings

---

Simple recipe to get some chicken and pesto.