---

title: Asian Glazed Salmon
image: asian-glazed-salmon.jpg
imageCredit: https://example.com
tags: salmon, rice, sweet, savory

categories:
- Dinner

ingredients:
- 6oz salmon
- heading: The Marinade
- 1 tsp fresh ginger, finely grated
- 1 garlic clove, crushed
- 1 tbsp soy sauce
- 2 tbsp oyster sauce
- 2 tbsp sweet chili sauce

directions:
- Combine marinade ingredients in a shallow bowl.
- Marinade the salmonsalmon.
- Cover and marinate for 30 minutes or up to overnight.
- Preheat oven to broil. Place the rack 25 cm / 10" from the heat source.
- Place salmon on baking tray (no oil required, no paper – it will burn). Dab glaze onto the salmon, whatever will stick. Don't pour excess glaze on, it will pool around the salmon and burn.
- Broil for 7 minutes.
- Remove, spray the surface generously with oil.
- Grill/broil for another 1 to 3 minutes until the surface is caramelised at the salmon is cooked – the flesh should flake.

tips:
- Heat the excess marinade to make safe and mix with rice.

prepTime: 5 minutes
cookTime: 10 minutes
totalTime: 50 minutes
makes: 2 servings

---
