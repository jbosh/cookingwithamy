---

title: Lettuce Wraps
image: lettuce-wraps.jpg
imageCredit: https://joyfoodsunshine.com/asian-chicken-lettuce-wraps/

tags: appetizer, whole 30, paleo

ingredients:
- 1 tablespoon olive oil
- ½ yellow onion diced
- 1 cup baby bella mushrooms minced
- 3 cloves garlic minced
- 1 pound ground pork or ground chicken
- "½ cup shredded carrots. *Recommended:* chopped if you want more crunch"
- salt and freshly ground black pepper
- ¼ cup coconut aminos
- 2 teaspoons sesame oil
- 1 teaspoon rice wine vinegar
- 1 tablespoon almond butter
- 2 teaspoons freshly grated ginger
- 1 tablespoon Whole30 compliant hot sauce sriracha
- 8-ounce can water chestnuts diced
- 3 green onions thinly sliced, 1 tablespoon reserved
- 1 head Bibb lettuce leaves gently removed from stem
- heading: Sauce
- ¼ cup coconut aminos
- 1½ teaspoons sesame oil
- 1 teaspoon rice wine vinegar
- Compliant hot sauce to taste
- 1 tablespoon green onions from above ingredients
- 2 teaspoons ginger
- 1 tablespoon of almond butter
- Salt.


directions:
- In a large skillet set over medium heat, heat olive oil.
- When the oil is hot, add onion and cook for about 3 minutes or until beginning to soften, then add mushrooms and cook 3 more minutes, or until onion is translucent and mushrooms are softening.
- Add garlic and cook just until fragrant, about 30 seconds, stirring constantly.
- Add ground pork and cook until browned, crumbling with a wooden spoon or spatula.
- Add carrots and stir until starting to soften, about 3 minutes.
- In a small bowl, combine ¼ cup coconut aminos, 2 teaspoons sesame oil, 1 teaspoon rice wine vinegar, almond butter, ginger, and hot sauce. Whisk until smooth then pour over the meat mixture. Stir until combined.
- Add the chestnuts and green onions. Cook for 3-4 minutes then remove from the heat. Season with salt and pepper to taste.
- heading: Sauce
- Whisk together all sauce ingredients in a small bowl.
- When ready to serve, spoon 3-4 tablespoons of the mixture into the center of a lettuce leaf and serve with sauce. Garnish with green onions and sesame seeds, if desired.

prepTime: 5 minutes
cookTime: 20 minutes
totalTime: 30 minutes
makes: 8 wraps
  
---

A healthy lettuce wraps recipe, inspired by the PF Chang's recipe but totally Whole30 compliant and paleo. Made with either pork or chicken, they're a great healthy Asian dinner recipe.