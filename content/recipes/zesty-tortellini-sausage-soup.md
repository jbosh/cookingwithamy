---

title: Zesty Tortellini Sausage Soup
image: zesty-tortellini-sausage-soup.jpg
imageCredit: https://www.lecreuset.com/zesty-tortellini-sausage-soup/LCR-2662.html
tags: dutch oven

categories:
- Dinner

ingredients:
- 1 tablespoon butter
- 1 tablespoon olive oil
- 3 spicy Italian sausages, casings removed
- 2 carrots, diced
- 2 ribs celery, diced
- 1 onion, diced
- 4 cloves garlic, minced
- 2 tablespoons thinly sliced fresh sage leaves
- &frac12; teaspoon crushed fennel seeds
- &frac12; teaspoon hot pepper flakes
- &frac12; teaspoon salt
- &frac12; teaspoon freshly cracked black pepper
- 2 tablespoons tomato paste
- 1 can (15-ounces) diced San Marzano tomatoes with juice
- 8 cups chicken broth
- 2 bay leaves
- 10 ounces prepared cheese tortellini pasta 
- 1 cup heavy cream
- 5 ounces baby spinach
- &frac12; cup grated pecorino cheese, divided
- &frac14; cup chopped fresh basil leaves

directions:
- In a Deep Dutch Oven set over medium heat, add butter and oil to the pot. Stir until butter melts.
- Crumble sausage into the pot, and cook, stirring occasionally, for 8-10 minutes or until sausage is starting to brown.
- Add carrots, celery, onion, garlic, sage, fennel seeds, hot pepper flakes, salt and pepper.
- Cook, stirring frequently, until vegetables start to soften, about 3-5 minutes.
- Add tomato paste and cook, stirring frequently, for 1 minute longer.
- Add tomatoes with juice, broth and bay leaves.
- Cover and bring liquid to a simmer. Reduce heat to medium-low. Cook, stirring occasionally, for 15-20 minutes or until vegetables are tender.
- Add tortellini and cream, and stir to combine.
- Cover and cook for 3-5 minutes or until tortellini are just cooked through.
- Add spinach and half of the cheese, and stir until spinach is wilted. Remove bay leaves from the pot and discard. Garnish bowls of soup with remaining cheese and basil for serving.

tips:
- Cook tortellini separately if you want it to remain al dente.

prepTime: 10 minutes
cookTime: 60 minutes
totalTime: 70 minutes
makes: 6 servings

storage:
  store: Refrigerate for up to 1 week.
  reheat: |
    Either:
      - Put in microwave for a couple minutes.
    
        **or**

      - If you cooked the tortellini separately, throw it all in a big pot and heat over low-med until it starts to boil.
  
---

Perfect for chilly fall and winter weeknights, this rich soup makes a hearty and comforting dinner that the entire family will enjoy. Serve alongside plenty of warm crusty bread. 