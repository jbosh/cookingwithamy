---

title: French Toast
image: french-toast.jpg
imageCredit: https://allrecipes.com
tags: breakfast, baking, bread, sweet
categories:
- breakfast

size: 8 slices

ingredients:
- 1 teaspoon ground cinnamon
- 1/4 teaspoon ground nutmeg
- 2 tablespoons sugar
- 4 eggs
- 1/4 cup milk
- 1/2 teaspoon vanilla extract
- 8 slices challah, brioche, or white bread
- 1/2 cup maple syrup, warmed

directions:
- In a small bowl, combine cinnamon, nutmeg, and sugar and set aside briefly.
- Whisk together cinnamon mixture, eggs, milk, and vanilla and pour into a shallow container such as a pie plate.
- In a skillet, melt butter over medium heat.
- Dip bread in egg mixture.
- Fry slices until golden brown, then flip it.

tips:
  - Leave your bread open for ~4 hours to let it stale up. Overnight was too long and fresh is too short. It makes the exterior soak up less while allowing the center to stay soft.

---
An excellent recipe when you're looking for that sweet breakfast fix.