---

title: Pasta Fazool
image: pasta-fazool.jpg
imageCredit: https://www.allrecipes.com/recipe/255823/pasta-fazool-pasta-e-fagioli/
tags: noodles, dinner, hamburger helper

categories:
- Dinner

ingredients:
- 1 tablespoon olive oil
- 12 ounces sweet bulk Italian sausage
- 1 stalk celery, diced
- ½ yellow onion, chopped
- ¾ cup dry elbow macaroni
- ¼ cup tomato paste
- 3 cups chicken broth, or more as needed, divided
- ¼ teaspoon crushed red pepper flakes, or to taste
- ¼ teaspoon dried oregano
- Salt and freshly ground black pepper to taste
- 3 cups chopped Swiss chard
- 1 (15 ounce) can cannellini (white kidney) beans, drained
- ¼ cup grated Parmigiano-Reggiano cheese, plus additional for serving, or to taste

directions:
- Heat oil in a skillet over medium-high heat. Cook and stir sausage in the hot skillet until browned and crumbly, about 5 minutes. Reduce heat to medium. Add diced celery and chopped onion. Cook until onions are translucent, 4 to 5 minutes. Add dry pasta; cook and stir for 2 minutes.
- Stir in tomato paste until evenly distributed, 2 to 3 minutes. Pour in 3 cups broth; increase heat to high and bring to a boil. Stir in red pepper flakes, oregano, salt, and pepper. Reduce heat to medium and let simmer, stirring often, for about 5 minutes. Add more broth if needed.
- Place chopped chard in a bowl. Cover with cold water and rinse leaves; any grit will fall to the bottom of the bowl. Transfer chard to a colander to drain briefly; add to soup. Cook and stir until leaves wilt, 2 to 3 minutes.
- Stir in white beans; continue cooking and stirring until pasta is tender, 4 to 5 minutes. Remove from heat and stir in grated cheese. Serve topped with additional grated cheese.
 
prepTime: 10 minute
cookTime: 25 minutes
totalTime: 35 minutes
makes: 2 servings
  
---

A dialed up hamburger helper straight from your childhood. Just a delicious cold weather meal that's almost a soup but hearty.