---

title: Easy Green Breakfast Bowl
image: easy-green-breakfast-bowl.jpg
imageCredit: https://www.paleorunningmomma.com/easy-green-breakfast-bowls-paleo-whole30/
tags: breakfest, paleo

categories:
- Breakfast

ingredients:
- 2 tablespoons olive oil
- 3 cups spinach chopped
- 2 cups shredded Brussels sprouts
- 2 cups cauliflower rice frozen or fresh
- 1 avocado
- Thinly sliced green onions or fresh chives
- Fresh lemon or lime juice
- Sea salt and freshly ground black pepper plus other favorite seasonings
- 2 eggs
- Ghee or olive oil to cook the eggs 

directions:
- Have your greens prepped and ready to go.
- Heat a large skillet with 2 tablespoons of ghee or olive oil.
- Once heated, add the shredded Brussels sprouts and cook about 30 seconds, then add the spinach and sprinkle with a tablespoon of water.
- Stir to cook down, then season with sea salt, black pepper, and any other seasonings you like.
- Remove from the skillet to two bowls and cover to keep warm.
- Add the cauliflower rice to the same skillet (you can add a bit more oil or ghee if needed) and season with salt, pepper, and a squeeze of lime juice.
- Once done, arrange around the greens in both bowls.
- Make a sunny side up egg.
- Peel and thinly slice your avocado and arrange over the greens and rice, top with the cooked eggs, and sprinkle all over with green onions. 

prepTime: 5 minute
cookTime: 10 minutes
totalTime: 15 minutes
makes: 2 servings
  
---

These green breakfast bowls are easy, healthy, filling and delicious! With lightly sautéed veggies like kale, shredded
Brussels sprouts and cauliflower rice perfectly seasoned, topped with sliced avocado, lime, greens onions and perfectly
cooked eggs. Fast and simple plus paleo, Whole30, and keto friendly.