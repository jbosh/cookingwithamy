---

title: Pesto Pasta Salad
image: pesto-pasta-salad.jpg
imageCredit: https://tastesbetterfromscratch.com/pesto-pasta-salad/
tags: salad, pesto

categories:
- Dinner

ingredients:
- 8 ounces farfalle pasta, whole wheat or white
- ¾ cup basil pesto, homemade or store bought
- 2 cups cherry tomatoes, halved (I use half red and half yellow)
- 2 mini cucumbers, sliced
- 3 ounces fresh mozzarella cheese, cut into 1/2-inch cubes
- ¼ cup freshly grated parmesan cheese

directions:
- Cook pasta according to package instructions. Drain and rinse with cold water.  
- Add pasta to a large bowl and stir in pesto. 
- Add tomatoes, cucumber and fresh mozzarella cubes and toss to combine.
- Top with parmesan cheese. Garnish with fresh cracked pepper, if desired.

prepTime: 10 minute
cookTime: 10 minutes
totalTime: 20 minutes
makes: 5 servings

storage:
  store: Refrigerate for up to 1 week. Store the pasta separately.
  reheat: Use fresh pasta.