---

title: Toum
image: toum.jpg
imageCredit: https://cooking.nytimes.com/recipes/1021075-toum-garlic-whip
tags: dipping sauce, toam, tome
prepTime: 15 minutes
totalTime: 24 hours
size: 4 cups

ingredients:
- 2 cup garlic cloves peeled *2-3 bulbs*
- 2 teaspoons Kosher salt
- 1 cup neutral oil such as canola or safflower
- &frac14; cup lemon juice (about 1 lemon)

directions:
- Slice the garlic cloves in half lengthwise and remove any green sprouts.
- Transfer the sliced garlic cloves into a food processor/blender and add the kosher salt to the garlic cloves. Process for a minute until the garlic becomes finely minced. Make sure to scrape down the sides of the food processor/blender afterwards.
- While the food processor/blender is running, slowly pour one to two tablespoons of oil, then stop and scrape down the bowl.
- Continue adding another tablespoon or two until the garlic starts looking creamy.
- Once the garlic looks emulsified by the few tablespoons of oil, increase the speed of pouring the oil and alternate with the ½ cup - of lemon juice until all the oil and lemon juice is incorporated. This will take about 15 minutes to complete.
- Transfer the sauce into a glass container and cover with a paper towel in the fridge overnight. Makes about 4 cups.
- The next day, replace the paper towel with an airtight lid and keep in the fridge for up to 3 months.

tips:
- Add more lemon if you want a little more zest. It's easy to overpower.
- Go slow adding ingredients, when the sauce gets to the desired consistency, stop adding oil.
- It takes about 2 days in the fridge to really settle, so wait until then before adding extra flavors.

storage:
  store: The sauce can be stored for up to 3 months in the fridge in an airtight container.
---

A delicious Lebanese dipping sauce that can be paired with naan, carrots, or anything that goes well with hummus.