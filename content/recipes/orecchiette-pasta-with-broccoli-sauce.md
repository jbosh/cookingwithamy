---

title: Orecchiette Pasta with Broccoli Sauce
image: orecchiette-pasta-with-broccoli-sauce.jpg
imageCredit: https://www.inspiredtaste.net/51372/applesauce/
tags: main, vegetarian
categories:
- Main

prepTime: 5 minutes
cookTime: 25 minutes
totalTime: 30 minutes
makes: 4 servings

ingredients:
- 12 ounces broccoli florets- cut or broken into very small florets about equal size (about 6 cups packed)
- 8 ounces short pasta - orecchiette pasta, penne, rigatoni, etc
- 2 tablespoons olive oil
- 6 garlic cloves *rough chopped*
- Generous pinch chili flakes
- Generous pinch salt and pepper *more to taste*
- "1 teaspoon miso paste **Alternative:** 1–2 mashed anchovies, or 4 tbsp of soy sauce
- 2 cups veggie broth or chicken broth
- 2 tbsp shaoxing wine
- "Garnish: grated parmesan or pecorino cheese,  lemon zest, chili flakes, fresh basil ribbons, toasted pine nuts, toasted bread crumbs, truffle oil or kalamata olives- all optional."


directions:
- Steam small broccoli florets until very tender (easily pierced with a fork) and set aside.
- Cook pasta according to directions, and save some hot pasta water when you drain.
- In a large pan, saute the garlic and chili flakes in the olive oil, over medium heat until fragrant and golden, about 2-3 minutes. Stir in the miso paste (or anchovy/soy sauce), then add steamed broccoli, salt, pepper and the broth.
- Bring to a gentle simmer and start breaking apart the broccoli with a metal spatula into tiny pieces. As it cooks it will get easier to break apart. You want the broccoli to basically melt down into a "sauce". Continue simmering gently on low heat until half of the liquid evaporates and it becomes the consistency of a thick sauce, about 10-15 minutes.
- If it seems watery and separated, just keep cooking it down. It won’t be smooth but just broken down and very very tender.
- Add the pasta and toss well. Add more pasta water if you like it a little more "saucy." Taste for salt, adding more along with pepper, chili flakes, and lemon zest.
- Divide among bowls and top with any of the garnishes you like. Grated Parmesan or pecorino adds depth and salt, lemon zest adds a nice brightness, which I highly recommend and a drizzle of truffle oil elevates.

---

Orecchiette Pasta with Broccoli Sauce - a simple Italian vegetarian pasta that is full of flavor and nutrients!