---

title: Chickpea and Kale Curry
image: chickpea-and-kale-curry.jpg
imageCredit: https://www.averiecooks.com/chickpea-kale-thai-coconut-curry/
tags: quick, Indian

categories:
- Dinner

ingredients:
- 1 yellow onion
- 4 tbsp minced garlic
- 1 tsp coriander
- ½ tsp turmeric 
- 1 tsp paprika 
- ¼ tsp smoked paprika
- 2 tbsp yellow curry paste
- ¼ tsp cinnamon
- 3 cans garbanzo beans
- 1 tbsp minced ginger
- 2 cups chopped kale
- 3 tbsp tomato paste 
- 1 can full fat coconut milk
- 1 tbsp cumin
- Salt and pepper to taste
- 1 cup veggie stock

directions:
- Sautée onion and garlic on high heat until soft.
- Put all other ingredients in Instant Pot.
- Pressure cook on High Pressure for 8 minutes.
- Quick release.
- Serve over rice.

prepTime: 8 minute
cookTime: 8 minutes
totalTime: 20 minutes
makes: 6 servings

storage:
  store: Refrigerate for up to 2 weeks.
  reheat: Put in microwave for a couple of minutes. Make fresh rice with each iteration.
  
tips:
- Let sit in the fridge overnight to let the flavors meld.

---

A Justin creation after getting tired of buying the Costco version.