---

title: Glazed Carrots
image: glazed-carrots.jpg
imageCredit: https://twokooksinthekitchen.com/roasted-honey-thyme-carrots/
tags: side

prepTime: 10 minutes
cookTime: 15 minutes
totalTime: 25 minutes
makes: 8 servings

ingredients:
- 2 pounds baby carrots
- ¼ cup butter
- ¼ cup packed brown sugar
- ¼ teaspoon salt
- ⅛ teaspoon ground white pepper

directions:
- Place carrots into a large saucepan; pour in enough water to reach depth of 1 inch and bring to a boil. Reduce heat to low, cover, and simmer carrots until tender, 5 to 8 minutes. Drain and transfer to a bowl.
- Melt butter in the same saucepan over low heat. Stir in brown sugar, salt, and white pepper until dissolved.
- Add carrots to the saucepan and toss until coated in the brown sugar mixture.
- Cook and stir until carrots are heated through and fully coated with glaze, 1 to 3 more minutes.

storage:
  store: Store in the fridge for up to 3-7 days.

---

Glazed carrot sticks coated in a mixture of brown sugar and butter are a versatile side dish you can serve alongside a weeknight meatloaf, a Sunday ham, or your Thanksgiving turkey.
These caramelized carrots are a definite kid-pleaser but they're elegant enough to impress the adults as well!

