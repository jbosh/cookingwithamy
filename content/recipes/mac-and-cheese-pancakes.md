---

title: Mac and Cheese Pancakes
image: mac-and-cheese-pancakes.jpg
imageCredit: https://www.foodnetwork.com/recipes/mac-and-cheese-pancakes-with-optional-hot-dog-8010647

tags: breakfast, habenero

ingredients:
- |
  1 lb of cheese. Mix and match, doesn't matter. Cheddar, mozzarella, you choose

  **Pro tip:** This recipe uses so much cheese. Get more than you think you need
- Pancake batter. Krusteaz is real good
- 1 cup of scallions, thinly sliced
- 2 cups of elbow macaroni
- Pack of breakfast sausages
- heading: Habanero Maple Syrup
- 1 habanero
- 1 cup of maple syrup


directions:
- heading: Habanero Maple Syrup
- Get the smallest pot you have.
- Remove seeds from habanero pepper.
- Roughly cut the habanero.
- Put habanero and syrup in pot.
- Heat on low for 5 minutes, stirring frequently. Syrup will burn on the bottom.
- heading: Pancakes
- Make pancake mix (exercise left for the reader).
- Cook elbow macaroni until it's al dente.
- Preheat the griddle low to medium heat.
- Put a ⅓ cup of pancake syrup on griddle.
- Cover pancake mix with macaroni and sausages.
- (optional) Put the scallions on the pancake.
- Cover with cheese.
- Wait until pancake starts to become cooked or the edges are bubbling.
- Flip pancake and wait until cheese is golden brown.


prepTime: 30 minutes
cookTime: 5 minutes
totalTime: 35 minutes
makes: 8 pancakes
  
---

A spectacular breakfast that sounds terrible but is incredible in practice.