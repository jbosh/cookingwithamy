---

title: Mediterranean Couscous Salad
image: mediterranean-couscous-salad.jpg
imageCredit: https://www.themediterraneandish.com/israeli-couscous-recipe/
tags: salad, couscous
categories:
- salad

size: 8 servings

ingredients:
- heading: Lemon-Dill Vinaigrette
- 1 large lemon, juice of
- ⅓ cup extra virgin olive oil (I used Greek Private Reserve)
- ½ tsp dill weed
- 1 to 2 garlic cloves, minced
- Salt and pepper (to taste)
- heading: Pearled Couscous
- 2 cups pearled couscous
- Extra virgin olive oil
- Garlic salt
- heading: Salad
- 2 cups grape tomatoes, halved
- ⅓ cup finely chopped red onions
- ½ English cucumber, finely chopped
- 15 oz can chickpeas, drained and rinsed
- 14 oz can artichoke hearts, roughly chopped if needed
- ½ cup pitted kalamata olives
- 15-20 fresh basil leaves, roughly chopped or torn; more for garnish
- 3 oz fresh baby mozzarella (or feta cheese), optional

directions:
- heading: Lemon-Dill Vinaigrette
- Mix all the vinaigrette into a bowl. Whisk together and set aside.
- heading: Pearled Couscous
- In a medium-sized pot.
- Head two tablespoons of olive oil.
- Saute the couscous in the olive oil briefly until golden brown.
- Add 3 cups of boiling water (or the amount instructed on the package), and cook according to package.
- When ready, there should be no waiter remaining. If there is, drain in a colander.
- Set aside in a bowl to cool.
- heading: Salad
- In a large mixing bowl, combine the remaining ingredients minus the basil and mozzarella.
- Add the couscous and the basil and mix together gently.
- Give the lemon-dill vinaigrette a quick whisk and add it to the salad.
- Mix in mozzarella and garnish with fresh basil.


tips:
  - Slightly warm couscous makes this recipe fantastic.

---
