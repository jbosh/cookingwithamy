---

title: Stuffed Bell Peppers
image: stuffed-bell-peppers.jpg
imageCredit: https://budgetbytes.com
tags: dinner, peppers, cheese

categories:
- Dinner

ingredients:
- 3 bell peppers
- 2 Tbsp cooking oil, divided
- 1 lb Italian sausage
- 1 yellow onion, diced
- 3 garlic cloves, minced
- 1 tsp Italian seasoning
- ½ tsp garlic powder
- 1¼ tsp salt, divided
- ¼ tsp freshly cracked black pepper
- 1 cup marinara sauce
- ½ cup uncooked long grain white rice
- ¾ cup chicken broth
- 1 cup shredded mozzarella

directions:
- Preheat the oven to 350°F. Wash and dry each bell pepper, then cut the bell peppers in half horizontally. Make sure to cut them as evenly as possible. Using a sharp paring knife carefully cut and remove the stem from the top half of each bell pepper (see picture below). It's okay if there is a small hole left where the stem was removed.
- Place each bell pepper half in a 9x13-inch casserole dish. Brush the bell peppers with 1 Tbsp oil and season with 1⁄4 tsp of salt and 1⁄4 tsp cracked black pepper. Bake the bell peppers in a preheated oven for 20 minutes to soften. After 20 minutes remove the bell peppers from the oven and set aside.
- While the bell peppers are baking, make the filling. Heat a large skillet over medium heat and add 1 Tbsp of oil. Brown the Italian sausage.
- Once the sausage has browned, add the diced onion and minced garlic to the skillet. Continue to sauté over medium heat until the onion is translucent and the garlic is fragrant.
- Next add the uncooked rice, marinara sauce, Italian seasoning, garlic powder, 1 tsp of salt, and chicken broth to the skillet. Stir to combine.
- Place a lid on the skillet, turn the heat up to medium-high, and allow the mixture to come to a full boil. Once boiling, immediately reduce the heat to medium-low and allow the mixture to simmer, without lifting the lid or stirring, for 20 minutes. After 20 minutes, turn the heat off and let it rest, without lifting the lid, for an additional 5 minutes.
- Next remove the lid, fluff the rice, and stir the mixture again to redistribute the ingredients. Begin to fill each bell pepper with the meat filling. Stuff as much filling as you can into each, filling them all the way to the top.
- Top each bell pepper evenly with shredded mozzarella cheese. Loosely place some tented aluminum foil over the top of the casserole dish and bake for 15 minutes. After 15 minutes the bell peppers should be tender but not mushy.
- Now remove the foil and turn the heat on to broil. Broil the stuffed bell peppers for 2-3 minutes or just until the cheese gets a little brown on top. Be sure to watch the bell peppers closely at this step to prevent the cheese from over browning. Garnish with parsley, if desired, and enjoy!

prepTime: 15 minutes
cookTime: 50 minutes
totalTime: 1 hour and 5 minutes
makes: 4 servings

---

These Stuffed Bell Peppers are a classic comfort food that’s hearty,
filling, has incredible flavors, and can easily feed a large family.