---

title: Steak Tacos
image: steak-tacos.jpg
imageCredit: https://www.cookingclassy.com/steak-tacos/

tags: tacos, slow cook

ingredients:
- 2 lbs flank steak (skirt works too)
- ¼ cup olive oil
- ¼ cup orange juice
- ¼ cup worcestershire sauce
- ¼ cup cilantro, chopped
- 1½ tsp minced garlic
- 1 Tbsp honey
- 1 tsp chili powder
- 1 tsp cumin


directions:
- Place your steak in the bottom of the crock pot.
- In a bowl mix together your olive oil, orange juice, worcestershire sauce, cilantro, garlic, honey, chili powder, and cumin then stir until blended.
- Pour over your meat and stir around slightly to distribute.
- Cover and cook on low for about 8 hours or until tender and meat shreds easily.


prepTime: 20 minutes
cookTime: 8 hours
totalTime: 9 hours
makes: 4 servings
  
---