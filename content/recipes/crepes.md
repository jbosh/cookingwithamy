---

title: Crepes
image: crepes.jpg
imageCredit: https://www.mygorgeousrecipes.com/perfect-french-crepes-recipe/
tags: breakfast
categories:
- Breakfast

ingredients:
- 1 ½ cups all-purpose flour
- 2 cups milk
- 2 tablespoons of sugar
- 2 eggs
- ½ teaspoon vanilla
- ½ teaspoon baking powder
- ½ teaspoon salt


directions:
- Put all these ingredients in a blender.
- Put that blend in the fridge for 4 hours (or overnight) until chilled.
- Put in a pan and make like you would make pancakes

---
