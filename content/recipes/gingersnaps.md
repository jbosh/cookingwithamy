---

title: Ginger Snaps
image: gingersnaps.jpg
imageCredit: https://vintagekitchennotes.com/gingersnap-cookies-recipe/

ingredients:
- 1 cup butter, at room temperature
- 1 cup packed brown sugar
- ½ cup unsulphured molasses
- 1 large egg
- 3½ cups flour
- 1 teaspoon baking soda
- ½ teaspoon salt
- 2 teaspoons ground ginger
- 1 teaspoon cinnamon
- ½ teaspoon ground cloves
- ½ teaspoon ground nutmeg
- ½ teaspoon freshly ground black pepper


directions:
- Beat butter and brown sugar together in a bowl with a mixer on medium speed. Mix in molasses, then egg, until blended, scraping bowl as needed.
- In a small bowl, combine flour, baking soda, salt, and spices; add to butter mixture on low speed, mixing until combined.
- Divide dough in half, shape each into a disk, wrap in plastic wrap, and chill until firm, about 3 hours.
- Preheat oven to 350°. Unwrap dough. On a generously floured surface, roll out each disk until 1/8 in. thick. Using round 2½ to 3-in. cookie cutters, cut out dough and arrange circles about 1 in. apart on parchment-lined baking sheets (use a small spatula to transfer). Reroll scraps as needed.
- Bake cookies until dry-looking and just starting to brown on edges, about 8 minutes. Set pans on racks and let cookies cool on pans.


tips:
- Dough requires refrigeration from 3 hours up to 1 week.
- Fresh ginger, either cut it yourself or get a new bottle. The cookies lost a lot of kick the following day.
- Broil the cookies at the end to make crispier edges.

prepTime: 30 minutes
cookTime: 8 minutes
totalTime: 4 hours
makes: 100 cookies
  
---

A delicious cookie recipe to make for the holidays.