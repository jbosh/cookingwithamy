---

title: Burrito Bowl
image: burrito-bowl.jpg
imageCredit: https://www.kitchenstories.com/en/recipes/veggie-burrito-bowl
tags: instant pot

ingredients:
- 1 pound boneless skinless chicken breasts *diced into bite sized pieces*
- 3 tablespoons of olive oil
- ¼ cup of diced yellow onion
- 1 cup of uncooked extra-long grain rice
- 1 14.5 oz can of fire roasted diced tomatoes drained
- 1 15 oz can of black beans drained and rinsed
- 1 teaspoon of garlic powder
- 1 teaspoon of onion powder
- ½ teaspoon of paprika
- 2 teaspoons of chili powder
- 1 teaspoon of cumin
- 2¼ cups of low-sodium chicken broth
- 2 cups of reduced fat colby jack monterey jack or cheddar cheese
- kosher salt and pepper
- freshly diced tomatoes
- diced green onions
- sour cream
- guacamole


directions:
- Add 1 tablespoon of olive oil to Instant Pot and hit the Saute button, then hit the adjust button to set to More.
- Season diced chicken with 1 teaspoon of kosher salt and 1/2 teaspoon of black pepper.
- Once oil is hot, add chicken to pot in two batches to encourage browning.
- Once chicken is browned, add onions and cook until they start to soften.
- Add additional tablespoon of olive oil and uncooked rice and toast uncooked rice for about 2 minutes or just until some grains start to turn golden brown. Don't skip this step. Rice needs to be toasted.
- Stir in black beans, canned tomatoes, chicken broth, garlic powder, chili powder, paprika and cumin. Once the liquid is added, use a wooden - spoon or spatula to scrape up browned bits from the bottom of the pot to prevent burn notice.
- Lock the Instant Pot lid in place and set to Manual, High Pressure for 7 minutes.
- Use Quick Pressure Release, and once pressure pin drops, open Instant Pot and fluff Chicken Burrito Bowl with fork.
- Sprinkle with cheese, recover and let set for 2-3 minutes to melt cheese.
- Garnish with fresh tomatoes, green onions, sour cream and guacamole.

prepTime: 10 minutes
cookTime: 7 minutes
totalTime: 20 minutes
makes: 6 servings
  
---

Simple healthy burrito bowl made in the instant pot.