import Path from "path";
import fs from "fs";
import * as GrayMatter from "gray-matter";

export interface Matter
{
  content: any;
  url: string;
  filename: string;
}

export function getMatterPath(directory: string): string;
export function getMatterPath(directory: string, filename: string): string
export function getMatterPath(directory: string, filename?: string): string
{
  if (filename)
    return Path.join(process.cwd(), "content", directory, filename);
  else
    return Path.join(process.cwd(), "content", directory);
}

function getFiles(directory: string): string[]
{
  let cwd = getMatterPath(directory);
  return fs.readdirSync(cwd, { withFileTypes: true })
    .filter(f => f.isFile())
    .map(f => Path.join(cwd, f.name));
}

export function getFilename(filename: string): string
{
  let path = Path.parse(filename);
  return path.name + path.ext;
}

export function getFilenameWithoutExtension(filename: string): string
{
  let path = Path.parse(filename);
  return path.name;
}

export function readMatter(fullPath: string): Matter
{
  let matter = GrayMatter.read(fullPath);
  return {
    ...matter.data,
    content: matter.content,
    url: getFilenameWithoutExtension(fullPath),
    filename: getFilename(fullPath),
  };
}

export function listMatterDirectory(directory: string): string[]
{
  let cwd = getMatterPath(directory);
  return fs.readdirSync(cwd, { withFileTypes: true })
    .filter(f => f.isFile())
    .map(f => f.name);
}

export function readMatterDirectory(directory: string): Matter[]
{
  return getFiles(directory).map(readMatter);
}