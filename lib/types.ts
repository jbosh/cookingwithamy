import { Matter } from "lib/offlineUtilities";

export interface StorageInformation
{
  /**
   * How to store or how long it will keep.
   */
  store: string | null;

  /**
   * How to reheat (if applicable).
   */
  reheat: string | null;

  /**
   * Instructions on proper freezing techniques and how long it will keep.
   */
  freeze: string | null;
}

export interface ItemList
{
  heading: string | null;
  items: string[];
}

export interface RecipeMatter extends Matter
{
  /**
   * Title of the post.
   */
  title: string;

  /**
   * If this recipe is a draft and shouldn't show up in searches or on the main page.
   */
  draft: boolean;

  /**
   * If this recipe has never been tested and doesn't get the Cooking with Amy seal of approval.
   */
  experimental: boolean;

  /**
   * Image path associated with recipe. It should be a relative path to `/public/images/`.
   */
  image: string;

  /**
   * URL to give credit to the original image (if required).
   */
  imageCredit: string | null;

  /**
   * List of tips for the recipe (in markdown).
   */
  tips: string[] | null;

  /**
   * List of tags that apply to this recipe. These are not displayed and only used for search.
   * You should use this to cover common typos and other issues in search also.
   */
  tags: string[] | null;

  /**
   * List of categories that apply to this recipe.
   */
  categories: string[] | null;

  /**
   * Amount of time required to prepare the meal.
   */
  prepTime: string | null;

  /**
   * Amount of time required for the meal to cook.
   */
  cookTime: string | null;

  /**
   * Total time until you can eat.
   */
  totalTime: string | null;

  /**
   * Serving size, or how much food this recipe makes.
   */
  size: string | null;

  /**
   * Information on how to store the recipe.
   */
  storage: StorageInformation | null;

  /**
   * List of components (in `/recipes/components/`) make up this recipe. This is useful if you have several sub
   * recipes required to make the complete recipe.
   */
  componentNames: string[] | null;

  /**
   * List of ingredients for the recipe. If an ingredient starts with `heading: ` it is special and
   * becomes a heading item. These can be full markdown.
   */
  ingredients: ItemList[] | null;

  /**
   * List of directions for the recipe. If an direction starts with `heading: ` it is special and
   * becomes a heading item. These can be full markdown.
   */
  directions: ItemList[] | null;
}

export interface PostMatter extends Matter
{
  title: string;
  author: string;
  date: Date | undefined;
  draft: boolean | null;
}

function prepareItemList(list: ItemList[] | null): ItemList[] | null
{
  if (!list)
    return null;

  let result = [] as ItemList[];
  let currentItem: ItemList = {
    heading: null,
    items: [],
  };

  for (let item of list as any[])
  {
    // Empty items in the list should be skipped.
    if (!item)
      continue;

    if (item.heading)
    {
      if (currentItem.heading !== null || currentItem.items.length !== 0)
        result.push(currentItem);

      currentItem = {
        heading: item.heading,
        items: [],
      };
    }
    else
    {
      currentItem.items.push(item);
    }
  }
  if (currentItem.heading !== null || currentItem.items.length !== 0)
    result.push(currentItem);

  return result.length !== 0 ? result : null;
}

export function prepareRecipe(matter: RecipeMatter): RecipeMatter
{
  matter.draft = matter.draft ?? false;
  matter.experimental = matter.experimental ?? false;
  matter.componentNames = (matter as any).components ?? null;

  matter.ingredients = prepareItemList(matter.ingredients);
  matter.directions = prepareItemList(matter.directions);

  return matter;
}