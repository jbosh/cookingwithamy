interface FooterProps
{
  recipeName?: string;
}

export default function Footer({ recipeName }: FooterProps)
{
  let newIssueLink = recipeName === undefined
    ? null
    : `issue[title]=${encodeURIComponent(`Recipe: ${recipeName}`)}`
  return <footer className="footer bg-darken-1 clearfix py4 mt3">
    <div className="container px3 sm-px4">
      <div className="clearfix">
        <div className="sm-col sm-col-6">
          <p className="left-align mb2">
            <a href="/search">Search</a> | <a href="/blog">Blog</a>
          </p>
        </div>
        <div className="sm-col sm-col-6"> <p className="right-align mb2">
          {newIssueLink &&
            <><a href={`https://gitlab.com/jbosh/cookingwithamy/-/issues/new?${newIssueLink}`}>
              Report Issue
            </a> | </>
          }
          <a href="https://gitlab.com/jbosh/cookingwithamy">Fork on Gitlab</a>
        </p>
        </div>
      </div>
    </div>
  </footer>;
}