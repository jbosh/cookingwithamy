export default function Header()
{
  return <a className="site-title fixed bg-blue p1 no-line-height top-0 left-0 z4" href="/">
    <img src="/assets/chowdown-logo-white.svg" alt="logo" />
    <h1 className="hide">Cooking with Amy</h1>
  </a>;
}